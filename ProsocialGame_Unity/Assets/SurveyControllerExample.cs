﻿using SimpleSurvey;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
//using UnityEngine.UI;

public class SurveyControllerExample : MonoBehaviour
{
    public SurveyData preTestSurveyData;
    public SurveyData postTestSurveyData;
    public SurveyData postDebriefSurveyData;
    public SurveyData postPlaytestSurveyData;
    private Survey preTestSurvey;
    private Survey postTestSurvey;
    private Survey postDebriefSurvey;
    private Survey postPlaytestSurvey;

    private DateTime studyStart;
    private DateTime forestStart;
    private DateTime stationStart;
    private DateTime stationEnd;
    private DateTime studyEnd;
    private DateTime firstTalkingStart = DateTime.MaxValue;
    private DateTime firstTalkingEnd = DateTime.MaxValue;
    private DateTime helpingEnd = DateTime.MaxValue;
    private DateTime secondTalkingStart = DateTime.MaxValue;
    private DateTime secondTalkingEnd = DateTime.MaxValue;
    private DateTime hordeStart;
    private DateTime postQuestionniareStart;

    private int forestKills = 0;
    private int forestShots = 0;
    private int forestDeaths = 0;

    private static List<Survey.SurveyKeyValuePair> surveyPairs = new List<Survey.SurveyKeyValuePair>();
    private static List<Survey.SurveyKeyValuePair> appendPairs = new List<Survey.SurveyKeyValuePair>();
    private static List<Survey.SurveyKeyValuePair> analyticsPairs = new List<Survey.SurveyKeyValuePair>();
    private static string sessionKey;

    public static string completionCode;

    [HideInInspector]
    public int randomisedCondition = 0;

    private void Awake() {
        DontDestroyOnLoad(gameObject);
    }

    //---------------------------------------------------------------------------------------------
    // Session setup.
    //---------------------------------------------------------------------------------------------
    void Start()
    {
        sessionKey = GenerateRandomString(20);
        completionCode = GenerateRandomString(5);
        StartCoroutine(DetermineCondition());
        string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH':'mm':'ss");
        surveyPairs.Add(new Survey.SurveyKeyValuePair("Time Started", dateTime));
        RecordStudyStart();

        ShowPreTestSurvey();
        //ShowPostTestSurvey();
        //ShowPostDebriefSurvey();
        //ShowPostPlaytestSurvey();

    }

    //---------------------------------------------------------------------------------------------
    // Show the pre test survey to the user.
    //---------------------------------------------------------------------------------------------
    public void ShowPreTestSurvey()
    {
        preTestSurvey = preTestSurveyData.BuildSurvey();
        preTestSurvey.AddEventListener(OnPreTestSurveyCompleted);

    }

    //---------------------------------------------------------------------------------------------
    // Show the post test survey to the user.
    //---------------------------------------------------------------------------------------------
    public void ShowPostTestSurvey ()
    {
        postTestSurvey = postTestSurveyData.BuildSurvey();
        postTestSurvey.AddEventListener(OnPostTestSurveyCompleted);
        RecordDataTime("PostQuestionnaireStarted");
        RecordPostQuestionnaireStart();
    }

    //---------------------------------------------------------------------------------------------
    // Show the post debrief survey to the user.
    //---------------------------------------------------------------------------------------------
    public void ShowPostDebriefSurvey ()
    {
        postDebriefSurvey = postDebriefSurveyData.BuildSurvey();
        postDebriefSurvey.AddEventListener(OnPostDebriefSurveyCompleted);
    }

    // //---------------------------------------------------------------------------------------------
    // // Show the playtest survey to the user.
    // //---------------------------------------------------------------------------------------------
    // public void ShowPostPlaytestSurvey ()
    // {
    //     postPlaytestSurvey = postPlaytestSurveyData.BuildSurvey();
    //     postPlaytestSurvey.AddEventListener(OnPostPlaytestSurveyCompleted);
    // }

    //---------------------------------------------------------------------------------------------
    // Add all pre-test survey responses to the stored data.
    //---------------------------------------------------------------------------------------------
    private void OnPreTestSurveyCompleted(Survey.SurveyKeyValuePair[] pairs)
    {
        foreach (Survey.SurveyKeyValuePair pair in pairs)
        {
            surveyPairs.Add(pair);
        }
        preTestSurvey.gameObject.SetActive(false);
        RecordGameStart();
        StartCoroutine(LoadNextLevel());
    }

    public void DeleteBeforeProlific(){
        StartCoroutine(LoadNextLevel());
    }
    
    //---------------------------------------------------------------------------------------------
    // Helper Function to move the game along once a survey is completed
    //---------------------------------------------------------------------------------------------
    public IEnumerator LoadNextLevel()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    //---------------------------------------------------------------------------------------------
    // Add all post-test survey responses to the stored data
    //---------------------------------------------------------------------------------------------
    private void OnPostTestSurveyCompleted(Survey.SurveyKeyValuePair[] pairs)
    {
        foreach (Survey.SurveyKeyValuePair pair in pairs)
        {
            surveyPairs.Add(pair);
        }
        
        ShowPostDebriefSurvey();
    }

    //---------------------------------------------------------------------------------------------
    // Add all post-debrief survey responses to the stored data then upload the data to the server.
    //---------------------------------------------------------------------------------------------
    private void OnPostDebriefSurveyCompleted(Survey.SurveyKeyValuePair[] pairs)
    {
        string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH':'mm':'ss");
        surveyPairs.Add(new Survey.SurveyKeyValuePair("Time Finished", dateTime));
        RecordStudyEnd();
        foreach (Survey.SurveyKeyValuePair pair in pairs)
        {
            surveyPairs.Add(pair);
        }

        CalculateTimes();
        CaptureGameAnalytics();
        CombineLists();
        StartCoroutine(UploadData());
    }

    //---------------------------------------------------------------------------------------------
    // Add all post-debrief survey responses to the stored data then upload the data to the server.
    //---------------------------------------------------------------------------------------------
    // private void OnPostPlaytestSurveyCompleted(Survey.SurveyKeyValuePair[] pairs)
    // {
    //     string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH':'mm':'ss");
    //     surveyPairs.Add(new Survey.SurveyKeyValuePair("Time Finished", dateTime));
    //     foreach (Survey.SurveyKeyValuePair pair in pairs)
    //     {
    //         surveyPairs.Add(pair);
    //     }

    //     CaptureGameAnalytics();
    //     StartCoroutine(UploadData());
    // }

    //---------------------------------------------------------------------------------------------
    // Game analytics functions to record game data at specific points
    //---------------------------------------------------------------------------------------------
    
    
    public void RecordData(string pairKey, string pairData){
        analyticsPairs.Add(new Survey.SurveyKeyValuePair(pairKey, pairKey + "; " + pairData));
    }

    public void RecordAppendData(string pairKey, string pairData){
        appendPairs.Add(new Survey.SurveyKeyValuePair(pairKey, pairData));
    }

    public void RecordDataTime(string pairKey){
        string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH':'mm':'ss");
        analyticsPairs.Add(new Survey.SurveyKeyValuePair(pairKey, pairKey + "; " + dateTime));
    }

    public void RecordStudyStart(){
        studyStart = DateTime.UtcNow;
    }

    public void RecordGameStart(){
        forestStart = DateTime.UtcNow;
    }

    public void RecordStationStart(){
        forestDeaths = GameManager.Instance.GetDeaths();
        forestKills = GameManager.Instance.GetTotalKills();
        forestShots = GameManager.Instance.GetBulletsFired();
        stationStart = DateTime.UtcNow;
    }

    public void RecordStationEnd(){
        stationEnd = DateTime.UtcNow;
    }

    public void RecordStudyEnd(){
        studyEnd = DateTime.UtcNow;
    }

    public void RecordFirstTalkingStart(){
        if(firstTalkingStart == DateTime.MaxValue){
            firstTalkingStart = DateTime.UtcNow;
        }
    }

    public void RecordFirstTalkingEnd(){
        if(firstTalkingEnd == DateTime.MaxValue){
            firstTalkingEnd = DateTime.UtcNow;
        }
    }

    public void RecordHelpingEnd(){
        helpingEnd = DateTime.UtcNow;
    }     
    
    public void RecordSecondTalkingStart(){
        if(secondTalkingStart == DateTime.MaxValue){
            secondTalkingStart = DateTime.UtcNow;        
        }
    }       
    
    public void RecordSecondTalkingEnd(){
        if(secondTalkingEnd == DateTime.MaxValue){
            secondTalkingEnd = DateTime.UtcNow;       
        }
    }

    public void RecordHordeStart(){
        hordeStart = DateTime.UtcNow;
    }
    public void RecordPostQuestionnaireStart(){
        postQuestionniareStart = DateTime.UtcNow;
    }  
    
    //---------------------------------------------------------------------------------------------
    // Combine the data lists into an aggregate file
    //---------------------------------------------------------------------------------------------
    public void CombineLists(){
        surveyPairs.AddRange(appendPairs);
        surveyPairs.AddRange(analyticsPairs);
    }

    //---------------------------------------------------------------------------------------------
    // Calculate the times a player was in the game
    //---------------------------------------------------------------------------------------------
    private void CalculateTimes(){
        RecordAppendData("TimeInStudy", (studyEnd - studyStart).ToString());
        RecordAppendData("TimeInGame", (stationEnd - forestStart).ToString());
        RecordAppendData("TimeInPreQuestionnaire", (forestStart - studyStart).ToString());        
        RecordAppendData("TimeInForest", (stationStart - forestStart).ToString());
        RecordAppendData("TimeInStation", (stationEnd - stationStart).ToString());
        RecordAppendData("TimeInPostQuestionnaire", (studyEnd - postQuestionniareStart).ToString());
    }


    //---------------------------------------------------------------------------------------------
    // Capture the game analytics data from the game manager
    //---------------------------------------------------------------------------------------------
    private void CaptureGameAnalytics(){
        GameManager gameManager = FindObjectOfType<GameManager>();
        RecordAppendData("#TotalKills", gameManager.GetTotalKills().ToString());
        RecordAppendData("#TotalBulletsFired", gameManager.GetBulletsFired().ToString());
        RecordAppendData("#TotalDeaths", gameManager.GetDeaths().ToString());
        RecordAppendData("#ForestKills", forestKills.ToString());
        RecordAppendData("#ForestBulletsFired", forestShots.ToString());
        RecordAppendData("#ForestDeath", forestDeaths.ToString());
        RecordAppendData("#StationKills", (gameManager.GetTotalKills() - forestKills).ToString());
        RecordAppendData("#StationBulletsFired", (gameManager.GetBulletsFired() - forestShots).ToString());
        RecordAppendData("#StationDeath", (gameManager.GetDeaths() - forestDeaths).ToString());
        RecordAppendData("TimeInFirstInteraction", (firstTalkingEnd - firstTalkingStart).ToString());
        RecordAppendData("TimeInGettingBottle", (helpingEnd - firstTalkingEnd).ToString());
        RecordAppendData("TimeInSecondInteraction", (secondTalkingEnd - secondTalkingStart).ToString());
        RecordAppendData("TimeFromHelpToStationEnd", (stationEnd - secondTalkingEnd).ToString());
        RecordAppendData("TimeFromHelpToHorde", (hordeStart - secondTalkingEnd).ToString());
        RecordAppendData("TimeFromHordeToStationEnd", (stationEnd - hordeStart).ToString());
        RecordData("StudyStartTime", studyStart.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("ForestStartTime", forestStart.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("StationStartTime", stationStart.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("StationEndTime", stationEnd.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("FirstTalkingStartTime", firstTalkingStart.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("FirstTalkingEndTime", firstTalkingEnd.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("HelpingEndTime", helpingEnd.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("SecondTalkingStartTime", secondTalkingStart.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("SecondTalkingEndTime",  secondTalkingEnd.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("HordeStartTime",  hordeStart.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("PostQuestionnaireStartTime", postQuestionniareStart.ToString("yyyy-MM-dd HH':'mm':'ss"));
        RecordData("StudyEndTime", studyEnd.ToString("yyyy-MM-dd HH':'mm':'ss"));
    }

    //---------------------------------------------------------------------------------------------
    // Upload all survey responses to the server.
    //---------------------------------------------------------------------------------------------
    IEnumerator UploadData()
    {
        // Generate the keys.
        string head = "";
        for (int i = 0; i < surveyPairs.Count; i++)
        {
            head += "\"" + surveyPairs[i].key + "\"";
            head += ",";
        }
        head += "\n";

        // Generate the values.
        string values = "";
        for (int i = 0; i < surveyPairs.Count; i++)
        {
            values += "\"" + surveyPairs[i].value + "\",";
        }
        values += "\n";

        // Send the log.
        WWWForm form = new WWWForm();
        form.AddField("head", head); // CSV headers.
        form.AddField("data", values); // CSV values.
        form.AddField("isdata", "1"); // File type. Set to 1 to generate an aggregate file of responses.
        form.AddField("name", sessionKey); // Name of file.
        form.AddField("folder", "prosocial"); // Folder file will go into.
        using (UnityWebRequest www = UnityWebRequest.Post("https://XXXXXXXXXXX.php", form)) //Enter a valid server address to send the log files to.
        {
            yield return www.SendWebRequest();
        }

        StartCoroutine(AddNewStopRecord());
    }

    //---------------------------------------------------------------------------------------------
    // Generate a random sequence of chacters.
    //---------------------------------------------------------------------------------------------
    public string GenerateRandomString(int count)
    {
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        string str = "";
        for (int i = 0; i < count; i++)
        {
            str += chars[UnityEngine.Random.Range(0, chars.Length)];
        }
        return str;
    }

    //---------------------------------------------------------------------------------------------
    // Returns the randomised condition of the current experiment for interal use.
    //---------------------------------------------------------------------------------------------
    public int GetRandomCondition(){
        return randomisedCondition;
    }


    //---------------------------------------------------------------------------------------------
    // Determine the randomised condition to set participants to.
    //---------------------------------------------------------------------------------------------

    public int timeSinceEpoch()
    {
        System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        int cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
        return cur_time;
    }


/// <summary>
/// Reads the database and increments an int in the conditions array for each completed
/// </summary>
    IEnumerator DetermineCondition ()
    {
        float EXPECTED_STUDY_LENGTH = 1800; // 30 minutes.
        UnityWebRequest www = UnityWebRequest.Get($"https://cinchdb.com/o3RL16FqbaEZiKxrUf4PMIgy52Wkt9/retrieve/csv");
        yield return www.SendWebRequest();
        string[] records = www.downloadHandler.text.Split('\n');

        int[] conditions = new int[3];
        foreach (string record in records)
        {
            string[] parts = record.Split(',');
            if (parts.Length > 4)
            {
                // If the user has finished the study, count them in the total samples for their condition.
                if (parts[3] == "1" || (int.Parse(parts[1]) + EXPECTED_STUDY_LENGTH > timeSinceEpoch())) 
                {
                    conditions[int.Parse(parts[2])]++;
                }
            }
        }

        // Find the condition with the minimum participants.
        int conditionToAssign = 0;
        for (int i = 0; i < conditions.Length; i++)
        {
            if (conditions[i] < conditions[conditionToAssign])
            {
                conditionToAssign = i;
            }
        }



        // PUT YOUR CODE HERE ... conditionToAssign will be 0, 1 or 2.
        randomisedCondition = conditionToAssign;
        RecordAppendData("RandomisedCondition", randomisedCondition.ToString());
        Debug.Log(randomisedCondition);
        yield return StartCoroutine(AddNewStartRecord(randomisedCondition));

    }

    IEnumerator AddNewStartRecord (int condition)
    {
        string time = timeSinceEpoch().ToString();
        UnityWebRequest www = UnityWebRequest.Get($"https://cinchdb.com/o3RL16FqbaEZiKxrUf4PMIgy52Wkt9/insert/{completionCode}/{time}/{condition}");
        yield return www.SendWebRequest();
    }

    IEnumerator AddNewStopRecord()
    {
        UnityWebRequest www = UnityWebRequest.Get($"https://cinchdb.com/o3RL16FqbaEZiKxrUf4PMIgy52Wkt9/update/col1={completionCode}/col4=1");
        yield return www.SendWebRequest();
    } 
}
