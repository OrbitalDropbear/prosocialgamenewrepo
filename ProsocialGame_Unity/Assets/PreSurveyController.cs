﻿using SimpleSurvey;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PreSurveyController : MonoBehaviour
{
    public SurveyData preTestSurveyData;
    private Survey preTestSurvey;

    private static List<Survey.SurveyKeyValuePair> surveyPairs = new List<Survey.SurveyKeyValuePair>();
    private static string sessionKey;

    //---------------------------------------------------------------------------------------------
    // Session setup.
    //---------------------------------------------------------------------------------------------
    void Start()
    {
        sessionKey = GenerateRandomString(20);
        ShowPreTestSurvey();
    }

    //---------------------------------------------------------------------------------------------
    // Show the pre test survey to the user.
    //---------------------------------------------------------------------------------------------
    public void ShowPreTestSurvey()
    {
        preTestSurvey = preTestSurveyData.BuildSurvey();
        preTestSurvey.AddEventListener(OnPreTestSurveyCompleted);

    }

    //---------------------------------------------------------------------------------------------
    // Add all pre-test survey responses to the stored data.
    //---------------------------------------------------------------------------------------------
    private void OnPreTestSurveyCompleted(Survey.SurveyKeyValuePair[] pairs)
    {
        string dateTime = DateTime.UtcNow.ToString("yyyy-MM-dd HH':'mm':'ss");
        surveyPairs.Add(new Survey.SurveyKeyValuePair("Time Started", dateTime));
        foreach (Survey.SurveyKeyValuePair pair in pairs)
        {
            surveyPairs.Add(pair);
        }
        preTestSurvey.gameObject.SetActive(false);
        StartCoroutine(UploadData());
    }


    //---------------------------------------------------------------------------------------------
    // Upload all survey responses to the server.
    //---------------------------------------------------------------------------------------------
    IEnumerator UploadData()
    {
        // Generate the keys.
        string head = "";
        for (int i = 0; i < surveyPairs.Count; i++)
        {
            head += "\"" + surveyPairs[i].key + "\"";
            head += ",";
        }
        head += "\n";

        // Generate the values.
        string values = "";
        for (int i = 0; i < surveyPairs.Count; i++)
        {
            values += "\"" + surveyPairs[i].value + "\",";
        }
        values += "\n";

        // Send the log.
        WWWForm form = new WWWForm();
        form.AddField("head", head); // CSV headers.
        form.AddField("data", values); // CSV values.
        form.AddField("isdata", "1"); // File type. Set to 1 to generate an aggregate file of responses.
        form.AddField("name", sessionKey); // Name of file.
        form.AddField("folder", "prosocial"); // Folder file will go into.
        using (UnityWebRequest www = UnityWebRequest.Post("https://XXXXXXXXXXX.php", form)) //Enter a valid server address to send the log files to.
        {
            yield return www.SendWebRequest();
        }
    }

    //---------------------------------------------------------------------------------------------
    // Generate a random sequence of chacters.
    //---------------------------------------------------------------------------------------------
    public string GenerateRandomString(int count)
    {
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
        string str = "";
        for (int i = 0; i < count; i++)
        {
            str += chars[UnityEngine.Random.Range(0, chars.Length)];
        }
        return str;
    }
}
