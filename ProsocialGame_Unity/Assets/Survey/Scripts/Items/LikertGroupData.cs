﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-------------------------------------------------------------------------------------------------
// Stores all data for the Likert group.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Likert Group", menuName = "Survey/Likert Group", order = 100)]
    public class LikertGroupData : SurveyItemData
    {
        //public string[] items;
        [Multiline]
        public string[] headers;

        //public string[] header
        public LikertGroupDataItem[] items;

        public float itemWidth = 100.0f;


        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            slotPrefab = template.likertGroup.gameObject;
           
            base.Build(template, parent);
        }

        [System.Serializable]
        public class LikertGroupDataItem
        {
            public bool required = true;
            public bool randomise = true;
            public string itemKey;
            public string item;
            
        }

        public override void AddToKeyList(List<string> keys)
        {
            foreach (LikertGroupDataItem item in items)
            {
                keys.Add(item.itemKey.Length > 0 ? item.itemKey : item.item);
            }
        }
    }
}
