﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SimpleSurvey
{
    public class SurveyPage : MonoBehaviour
    {
        [HideInInspector] public RectTransform rectTrans;
        [HideInInspector] public Survey surveyObj;

        public RectTransform contentContainer;

        public TextMeshProUGUI headerText;
        public TextMeshProUGUI descriptionText;

        public TextMeshProUGUI progressText;
        public Image progressImage;

        public Button backButton;
        public Button forwardButton;
        public GameObject errorWarning;

        private SurveyPageData surveyData;

        private float cGroupAlpha = -0.25f;

        private void Awake()
        {
            rectTrans = GetComponent<RectTransform>();
        }

        private void Update()
        {
            cGroupAlpha += Time.deltaTime;
            GetComponent<CanvasGroup>().alpha = cGroupAlpha;
        }

        //---------------------------------------------------------------------------------------------
        // 
        //---------------------------------------------------------------------------------------------
        public void OnPreviousButtonPressed()
        {
            surveyObj.PreviousPage();
        }

        //---------------------------------------------------------------------------------------------
        // 
        //---------------------------------------------------------------------------------------------
        public void OnNextButtonPressed()
        {
            
            surveyObj.NextPage();
        }

        //---------------------------------------------------------------------------------------------
        // Return true if the page has been successfully completed, otherwise false.
        //---------------------------------------------------------------------------------------------
        public bool IsPageCompleted()
        {
            SurveyItem[] surveyItems = GetComponentsInChildren<SurveyItem>();
            foreach (SurveyItem surveyItem in surveyItems)
            {
                if (!surveyItem.IsValidSubmission())
                {
                    return false;
                }
            }
            return true;
        }

        public void Setup(SurveyPageData data, int page, int totalPages)
        {
            this.surveyData = data;
            float progress = (float)page / totalPages;
            headerText.text = data.surveyPageTitle;
            if (headerText.text.Length == 0) headerText.gameObject.SetActive(false);
            descriptionText.text = data.surveyPageDescription;
            if (descriptionText.text.Length == 0) descriptionText.gameObject.SetActive(false);
            progressText.text = ((int)(progress * 100.0f)) + "%";
            progressImage.fillAmount = progress;

            if (!data.allowBackPress || page == 0) backButton.gameObject.SetActive(false);
            if (page == totalPages - 1)
            {
                //forwardButton.GetComponentInChildren<TextMeshProUGUI>().text = "Submit";
            }

            if (!data.allowForwardPress)
            {
                forwardButton.gameObject.SetActive(false);
            }
        }
    }
}