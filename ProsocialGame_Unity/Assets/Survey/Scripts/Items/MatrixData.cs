﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-------------------------------------------------------------------------------------------------
// Stores all data for the matrix.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Matrix", menuName = "Survey/Matrix", order = 100)]
    public class MatrixData : SurveyItemData
    {
        //public string[] items;
        public string[] headers;

        public MatrixDataItem[] items;

        [System.Serializable]
        public class MatrixDataItem
        {
            public string itemKey;
            public string item;
            public bool randomize = true;
        }

        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            slotPrefab = template.matrix.gameObject;
            base.Build(template, parent);
        }

        public override void AddToKeyList(List<string> keys)
        {
            foreach (MatrixDataItem item in items)
            {
                keys.Add(item.itemKey.Length > 0 ? item.itemKey : item.item);
            }
        }
    }
}