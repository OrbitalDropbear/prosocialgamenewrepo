﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SimpleSurvey
{
    public class Input : SurveyItem
    {
        public TextMeshProUGUI description;
        public TMP_InputField inputField;

        public override void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            base.Setup(slot, template);
            //description.text = (SurveyData.nextQuestionNumber++) + ". " + slot.description + RequiredExtra();
            description.text = slot.description + RequiredExtra();

            InputData data = (InputData)slot;
            if (data.inputValidation == InputData.InputValidation.Integer)
            {
                inputField.contentType = TMP_InputField.ContentType.IntegerNumber;
            }
            else if (data.inputValidation == InputData.InputValidation.Decimal)
            {
                inputField.contentType = TMP_InputField.ContentType.DecimalNumber;
            }
            else if (data.inputValidation == InputData.InputValidation.Password)
            {
                inputField.contentType = TMP_InputField.ContentType.Password;
            }
            else if (data.inputValidation == InputData.InputValidation.Email)
            {
                inputField.contentType = TMP_InputField.ContentType.EmailAddress;
            }
            inputField.characterLimit = data.characterLimit;

            if (data.inputSize == InputData.InputSize.Small)
            {
                Vector2 size = inputField.GetComponent<RectTransform>().sizeDelta;
                size.x = data.width;
                inputField.GetComponent<RectTransform>().sizeDelta = size;
            }
        }

        //-----------------------------------------------------------------------------------------
        // Return true if the field either isn't required or data has been entered.
        //-----------------------------------------------------------------------------------------
        public override bool IsValidSubmission()
        {
            return !(attachedData.required && inputField.text.Length == 0);
        }

        //-----------------------------------------------------------------------------------------
        // Return whatever text has been entered into the input field.
        //-----------------------------------------------------------------------------------------
        public override string GetValue()
        {
            return inputField.text + " %*&#$";
        }
    }
}


