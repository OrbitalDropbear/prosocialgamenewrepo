﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SimpleSurvey
{
    public class RadioVertical : SurveyItem
    {
        public GameObject radioTemplate;

        public override void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            base.Setup(slot, template);
            RadioVerticalData data = (RadioVerticalData)slot;
            GetComponentInChildren<TextMeshProUGUI>().text = data.description + RequiredExtra(); //(SurveyData.nextQuestionNumber++) + ". " +

            RectTransform rectTrans = GetComponent<RectTransform>();
            for (int i = 0; i < data.items.Length; i++)
            {
                RectTransform r = Instantiate(radioTemplate, rectTrans).GetComponent<RectTransform>();
                r.GetComponentInChildren<TextMeshProUGUI>().text = data.items[i];
                r.GetComponentInChildren<Toggle>().group = GetComponent<ToggleGroup>();

                if(i == 0 && data.preSelectFirst){
                    r.GetComponentInChildren<Toggle>().isOn = true;
                }
            }

            if (data.noneOfTheAbove)
            {
                RectTransform r = Instantiate(radioTemplate, rectTrans).GetComponent<RectTransform>();
                r.GetComponentInChildren<TextMeshProUGUI>().text = "None of the above";
                r.GetComponentInChildren<Toggle>().group = GetComponent<ToggleGroup>();
            }

            if (data.allOfTheAbove)
            {
                RectTransform r = Instantiate(radioTemplate, rectTrans).GetComponent<RectTransform>();
                r.GetComponentInChildren<TextMeshProUGUI>().text = "All of the above";
                r.GetComponentInChildren<Toggle>().group = GetComponent<ToggleGroup>();
            }

            if (data.otherPleaseSpecify)
            {
                RectTransform r = Instantiate(radioTemplate, rectTrans).GetComponent<RectTransform>();
                r.GetComponentInChildren<TextMeshProUGUI>().text = "Other, please specify: ";
                r.GetComponentInChildren<Toggle>().group = GetComponent<ToggleGroup>();
            }
            
            DestroyImmediate(radioTemplate);
        }

        //-----------------------------------------------------------------------------------------
        // If item is required, the submission is only valid if at least one checkbox is ticked.
        //-----------------------------------------------------------------------------------------
        public override bool IsValidSubmission()
        {
            if (!attachedData.required) return true;
            Toggle[] toggles = gameObject.GetComponentsInChildren<Toggle>();
            foreach (Toggle toggle in toggles)
            {
                if (toggle.isOn)
                {
                    return true;
                }
            }
            return false;
        }

        //-----------------------------------------------------------------------------------------
        // Return a string containing all selected items (bar separated).
        //-----------------------------------------------------------------------------------------
        public override string GetValue()
        {
            Toggle[] toggles = gameObject.GetComponentsInChildren<Toggle>();
            foreach (Toggle toggle in toggles)
            {
                if (toggle.isOn)
                {
                    return toggle.GetComponentInChildren<TextMeshProUGUI>().text;
                }
            }
            return "";
        }

        
    }
}