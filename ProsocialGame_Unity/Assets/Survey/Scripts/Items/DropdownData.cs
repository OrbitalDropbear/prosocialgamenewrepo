﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-------------------------------------------------------------------------------------------------
// Stores all data for the dropdown survey item.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Dropdown", menuName = "Survey/Dropdown", order = 100)]
    public class DropdownData : SurveyItemData
    {
        public string[] options;

        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            slotPrefab = template.dropdown.gameObject;
            base.Build(template, parent);
        }
    }
}
