﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Survey Page", menuName = "Survey/Survey Page", order = 52)]
    public class SurveyPageData : ScriptableObject
    {
        public string surveyPageTitle;

        [Multiline]
        public string surveyPageDescription;

        public SurveyItemData[] surveySlots;

        public bool allowBackPress = true;
        public bool allowForwardPress = true;
        public bool randomise = false;

        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public void Build(SurveyTemplate template, RectTransform parent)
        {
            SurveyPage obj = Instantiate(template.surveyPage, parent).GetComponent<SurveyPage>();
            obj.rectTrans.anchoredPosition = new Vector3(0, 0);
        }
    }
}
