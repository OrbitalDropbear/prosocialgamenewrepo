﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SimpleSurvey
{
    public class Checkbox : SurveyItem
    {
        public GameObject checkboxTemplate;

        public override void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            base.Setup(slot, template);
            CheckboxData data = (CheckboxData)slot;
            GetComponentInChildren<TextMeshProUGUI>().text = (SurveyData.nextQuestionNumber++) + ". " + data.description + RequiredExtra();

            RectTransform rectTrans = GetComponent<RectTransform>();
            for (int i = 0; i < data.items.Length; i++)
            {
                RectTransform r = Instantiate(checkboxTemplate, rectTrans).GetComponent<RectTransform>();
                r.GetComponentInChildren<TextMeshProUGUI>().text = data.items[i];
            }

            if (data.noneOfTheAbove)
            {
                RectTransform r = Instantiate(checkboxTemplate, rectTrans).GetComponent<RectTransform>();
                r.GetComponentInChildren<TextMeshProUGUI>().text = "None of the above";
            }

            if (data.allOfTheAbove)
            {
                RectTransform r = Instantiate(checkboxTemplate, rectTrans).GetComponent<RectTransform>();
                r.GetComponentInChildren<TextMeshProUGUI>().text = "All of the above";
            }

            if (data.otherPleaseSpecify)
            {
                RectTransform r = Instantiate(checkboxTemplate, rectTrans).GetComponent<RectTransform>();
                r.GetComponentInChildren<TextMeshProUGUI>().text = "Other, please specify: ";
            }

            DestroyImmediate(checkboxTemplate);
        }

        //-----------------------------------------------------------------------------------------
        // If item is required, the submission is only valid if at least one checkbox is ticked.
        //-----------------------------------------------------------------------------------------
        public override bool IsValidSubmission()
        {
            if (!attachedData.required) return true;
            Toggle[] toggles = gameObject.GetComponentsInChildren<Toggle>();
            foreach (Toggle toggle in toggles)
            {
                if (toggle.isOn)
                {
                    return true;
                }
            }
            return false;
        }

        //-----------------------------------------------------------------------------------------
        // Return a string containing all selected items (bar separated).
        //-----------------------------------------------------------------------------------------
        public override string GetValue()
        {
            string result = "";
            string separator = "|";

            Toggle[] toggles = gameObject.GetComponentsInChildren<Toggle>();
            foreach (Toggle toggle in toggles)
            {
                if (toggle.isOn)
                {
                    result += toggle.GetComponentInChildren<TextMeshProUGUI>().text + separator;
                }
            }
            if (result.Length > 0) result = result.Remove(result.Length - 1);
            return result;
        }
    }
}

