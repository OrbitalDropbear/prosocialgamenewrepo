﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SimpleSurvey
{
    public class RankOrder : SurveyItem
    {
        public TextMeshProUGUI description;
        public RectTransform rankHeader;
        public RectTransform rankItem;

        public override void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            base.Setup(slot, template);
            RankOrderData data = (RankOrderData)slot;

            // Set the matrix description.
            description.text = data.description;

            // Build the header.
            ToggleGroup[] toggleGroups = new ToggleGroup[data.items.Length];
            GameObject temp = rankHeader.GetComponentInChildren<TextMeshProUGUI>().gameObject;
            for (int i = 0; i < data.items.Length; i++)
            {
                GameObject go = Instantiate(temp, rankHeader);
                go.name += i.ToString();
                go.GetComponent<TextMeshProUGUI>().text = (i + 1).ToString();
                toggleGroups[i] = go.AddComponent<ToggleGroup>();
            }
            Destroy(temp);

            // Build each of the items.
            for (int i = 0; i < data.items.Length; i++)
            {
                GameObject item = Instantiate(rankItem.gameObject, GetComponent<RectTransform>());

                //
                item.GetComponentInChildren<Image>(true).color = i % 2 == 0 ? template.evenHighlight : template.oddHighlight;

                // Assign the label for the item.
                item.GetComponentInChildren<TextMeshProUGUI>().text = data.items[i];
                temp = item.GetComponentInChildren<Toggle>().gameObject;

                // Create the correct number of checkboxes.
                for (int j = 0; j < data.items.Length; j++)
                {
                    Instantiate(temp, temp.GetComponent<RectTransform>().parent).GetComponent<Toggle>().group = toggleGroups[j];
                }
                Destroy(temp);
            }
            Destroy(rankItem.gameObject);
        }
    }
}
