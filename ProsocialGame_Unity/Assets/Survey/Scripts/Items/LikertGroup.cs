﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SimpleSurvey
{
    public class LikertGroup : SurveyItem
    {
        public TextMeshProUGUI description;
        public RectTransform likertHeader;
        public RectTransform likertItem;

        public TextMeshProUGUI questionNumberText;

        private GameObject[] items;

        private LikertGroupData.LikertGroupDataItem[] randomisedItems;

        public LikertGroupData.LikertGroupDataItem[] RandomiseItems(LikertGroupData.LikertGroupDataItem[] items)
        {
            // Rebuild the array.
            LikertGroupData.LikertGroupDataItem[] randomisedItems = new LikertGroupData.LikertGroupDataItem[items.Length];
            for (int i = 0; i < randomisedItems.Length; i++)
            {
                randomisedItems[i] = items[i];
            }

            // Randomise all elements which can be randomised.
            for (int i = 0; i < randomisedItems.Length; i++)
            {
                if (randomisedItems[i].randomise)
                {
                    while (true)
                    {
                        int r = Random.Range(0, randomisedItems.Length);
                        if (randomisedItems[r].randomise)
                        {
                            LikertGroupData.LikertGroupDataItem temp = randomisedItems[r];
                            randomisedItems[r] = randomisedItems[i];
                            randomisedItems[i] = temp;
                            break;
                        }
                    }
                }
            }
            return randomisedItems;
        }

        public override void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            base.Setup(slot, template);
            LikertGroupData data = (LikertGroupData)slot;

            Vector2 size = likertHeader.GetComponentInChildren<TextMeshProUGUI>().GetComponent<RectTransform>().sizeDelta;
            size.x = data.itemWidth;
            likertHeader.GetComponentInChildren<TextMeshProUGUI>().GetComponent<RectTransform>().sizeDelta = size;

            size = likertItem.GetComponentInChildren<Toggle>().GetComponent<RectTransform>().sizeDelta;
            size.x = data.itemWidth;
            likertItem.GetComponentInChildren<Toggle>().GetComponent<RectTransform>().sizeDelta = size;

            // Set the matrix description.
            description.text = data.description + RequiredExtra();

            // Build the header.
            GameObject temp = likertHeader.GetComponentInChildren<TextMeshProUGUI>().gameObject;
            for (int i = 0; i < data.headers.Length; i++)
            {
                Instantiate(temp, likertHeader).GetComponent<TextMeshProUGUI>().text = data.headers[i];
            }
            Destroy(temp);

            // Build each of the items.
            
            items = new GameObject[data.items.Length];
            randomisedItems = RandomiseItems(data.items);
            for (int i = 0; i < randomisedItems.Length; i++)
            {
                items[i] = Instantiate(likertItem.gameObject, GetComponent<RectTransform>());

                //
                items[i].GetComponentInChildren<Image>(true).color = i % 2 == 0 ? template.evenHighlight : template.oddHighlight;

                // Assign the label for the item.
                items[i].GetComponentsInChildren<TextMeshProUGUI>(true)[0].text = (SurveyData.nextQuestionNumber++) + ". ";
                items[i].GetComponentsInChildren<TextMeshProUGUI>(true)[1].text = randomisedItems[i].item;
                temp = items[i].GetComponentInChildren<Toggle>().gameObject;

                // Create the correct number of checkboxes.
                for (int j = 0; j < data.headers.Length; j++)
                {
                    GameObject newToggle = Instantiate(temp, temp.GetComponent<RectTransform>().parent);
                    newToggle.GetComponent<Toggle>().group = newToggle.GetComponentInParent<ToggleGroup>();
                }
                Destroy(temp);
                LayoutRebuilder.ForceRebuildLayoutImmediate(items[i].GetComponent<RectTransform>());
            }
            Destroy(likertItem.gameObject);

            // Rebuild the layout.
            LayoutRebuilder.ForceRebuildLayoutImmediate(description.GetComponent<RectTransform>());
        }


        public override bool IsValidSubmission()
        {
            for (int i = 0; i < items.Length; i++)
            {
                bool toggleEnabled = false;
                Toggle[] toggles = items[i].GetComponentsInChildren<Toggle>();
                foreach (Toggle toggle in toggles)
                {
                    if (toggle.isOn)
                    {
                        toggleEnabled = true;
                    }
                }

                if (!toggleEnabled && randomisedItems[i].required)
                {
                    return false;
                }
            }
            return true;
        }


        //-----------------------------------------------------------------------------------------
        // 
        //-----------------------------------------------------------------------------------------
        public override void AddToResultsDictionary(Dictionary<string, string> resultsDictionary)
        {
            LikertGroupData data = (LikertGroupData)attachedData;
            for (int i = 0; i < randomisedItems.Length; i++)
            {
                // Build the key.
                string key = randomisedItems[i].itemKey.Length > 0 ? randomisedItems[i].itemKey : randomisedItems[i].item;


                bool allHeadersShown = true;
                foreach (string header in data.headers)
                {
                    if (header.Length == 0)
                    {
                        allHeadersShown = false;
                    }
                }

                // Build the value.
                string value = "";
                Toggle[] toggles = items[i].GetComponentsInChildren<Toggle>();
                for (int j = 0; j < toggles.Length; j++)
                {
                    if (toggles[j].isOn)
                    {
                        if (allHeadersShown)
                        {
                            value = (j+1).ToString();
                            //value = data.headers[j];
                        }
                        else
                        {
                            value = (j+1).ToString();
                        }
                    }
                }
                resultsDictionary.Add(key, value);
            }
        }
    }
}