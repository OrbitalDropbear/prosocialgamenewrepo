﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SimpleSurvey
{
    public class Container : SurveyItem
    {
        public override void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            base.Setup(slot, template);
            ContainerData data = (ContainerData)slot;
            if (!data.containerIsVisible) GetComponent<Image>().enabled = false;
        }

        //-----------------------------------------------------------------------------------------
        // Containers are always valid.
        //-----------------------------------------------------------------------------------------
        public override bool IsValidSubmission()
        {
            return true;
        }

        //-----------------------------------------------------------------------------------------
        // Container keys are always empty.
        //-----------------------------------------------------------------------------------------
        public override string GetKey()
        {
            return "";
        }

        //-----------------------------------------------------------------------------------------
        // Container values are always empty.
        //-----------------------------------------------------------------------------------------
        public override string GetValue()
        {
            return "";
        }
    }
}
