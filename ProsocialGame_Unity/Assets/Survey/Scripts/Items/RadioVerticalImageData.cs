﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-------------------------------------------------------------------------------------------------
// Stores all data for the vertical radial survey item.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Radio Image Item (Vertical)", menuName = "Survey/Radio Image Item (Vertical)", order = 100)]
    public class RadioVerticalImageData : SurveyItemData
    {
        public Sprite[] items;

        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            slotPrefab = template.radialVerticalImage.gameObject;
            base.Build(template, parent);
        }
    }
}
