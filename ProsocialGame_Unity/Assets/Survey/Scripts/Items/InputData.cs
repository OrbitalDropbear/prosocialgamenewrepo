﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-------------------------------------------------------------------------------------------------
// Stores all data for the small input survey item.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Input", menuName = "Survey/Input", order = 100)]
    public class InputData : SurveyItemData
    {
        public InputSize inputSize;
        public InputValidation inputValidation;
        public int characterLimit = 0;
        public int width = 300;

        public enum InputSize
        {
            Small,
            OneLine,
            MultiLine,
            Branch
        }

        public enum InputValidation
        {
            Integer,
            Decimal,
            TextOnly,
            NoRestriction,
            Password,
            Email
        }

        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            if (inputSize == InputSize.Small)
            {
                slotPrefab = template.inputSmall.gameObject;
            }
            else if (inputSize == InputSize.OneLine)
            {
                slotPrefab = template.inputOneLine.gameObject;
            }
            else if (inputSize == InputSize.MultiLine)
            {
                slotPrefab = template.inputMultiLine.gameObject;
            }
            else if (inputSize == InputSize.Branch)
            {
                slotPrefab = template.branchInput.gameObject;
            }
            base.Build(template, parent);
        }
    }
}
