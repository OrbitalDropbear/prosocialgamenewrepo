﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Rank Order Item", menuName = "Survey/Rank Order", order = 100)]
    public class RankOrderData : SurveyItemData
    {
        public string[] items;

        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            slotPrefab = template.rankOrder.gameObject;
            base.Build(template, parent);
        }
    }
}
