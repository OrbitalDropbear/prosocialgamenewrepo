﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SimpleSurvey;

public class BranchedInput : SimpleSurvey.Input
{
    string valueToBranch = "Yes";
    RadioVertical branchFromItem;

    private void Start() {
        branchFromItem = GameObject.FindObjectOfType<RadioVertical>();

        Toggle[] branchToggles = branchFromItem.GetComponentsInChildren<Toggle>();

        for(int i = 0; i < branchToggles.Length; i++){
            branchToggles[i].onValueChanged.AddListener(delegate {
                ToggleValueChanged();
            });

        }
        
        for(int i = 0; i < transform.childCount; i++){
                transform.GetChild(i).gameObject.SetActive(false);
        }

        inputField.text = "null";
    }

    void ToggleValueChanged()
    {

        if(branchFromItem.GetValue() == valueToBranch){
            inputField.text = "";
            for(int i = 0; i < transform.childCount; i++){
                transform.GetChild(i).gameObject.SetActive(true);
            }
            
        } else if(branchFromItem.GetValue() != valueToBranch){
            inputField.text = "null";
            for(int i = 0; i < transform.childCount; i++){
                transform.GetChild(i).gameObject.SetActive(false);
            }
            
        }
    }
    
}
