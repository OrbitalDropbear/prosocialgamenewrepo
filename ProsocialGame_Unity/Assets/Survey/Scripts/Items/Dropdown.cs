﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//-------------------------------------------------------------------------------------------------
// Handles all logic for the survey item game object.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    public class Dropdown : SurveyItem
    {
        public TextMeshProUGUI description;
        public TMP_Dropdown dropdownPrefab;

        public override void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            base.Setup(slot, template);
            DropdownData data = (DropdownData)slot;
            //description.text = (SurveyData.nextQuestionNumber++) + ". " + slot.description + RequiredExtra();
            description.text = data.description + RequiredExtra(); //(SurveyData.nextQuestionNumber++) + ". " +
            List<string> customOptions = new List<string>();
            
            for (int i = 0; i < data.options.Length; i++){
                customOptions.Add(data.options[i]);
            }
            dropdownPrefab.ClearOptions();
            dropdownPrefab.AddOptions(customOptions);
        }

        //-----------------------------------------------------------------------------------------
        // If item is required, the submission is only valid if at least one checkbox is ticked.
        //-----------------------------------------------------------------------------------------
        public override bool IsValidSubmission()
        {
            return true;
        }

        //-----------------------------------------------------------------------------------------
        // Return a string containing all selected items (bar separated).
        //-----------------------------------------------------------------------------------------
        public override string GetValue()
        {
            return dropdownPrefab.options[dropdownPrefab.value].text;
        }
    }
}

