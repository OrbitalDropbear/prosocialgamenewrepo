﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-------------------------------------------------------------------------------------------------
// Stores all data for the vertical checkbox.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Checkbox", menuName = "Survey/Checkbox", order = 100)]
    public class CheckboxData : SurveyItemData
    {
        public string[] items;
        public bool noneOfTheAbove;
        public bool allOfTheAbove;
        public bool otherPleaseSpecify;

        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            slotPrefab = template.checkbox.gameObject;
            base.Build(template, parent);
        }
    }
}
