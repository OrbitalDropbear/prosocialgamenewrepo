﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-------------------------------------------------------------------------------------------------
// Stores all data for the vertical radial survey item.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Radio Item (Vertical)", menuName = "Survey/Radio Item (Vertical)", order = 100)]
    public class RadioVerticalData : SurveyItemData
    {
        public string[] items;
        public bool noneOfTheAbove;
        public bool allOfTheAbove;
        public bool otherPleaseSpecify;
        public bool preSelectFirst;

        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            slotPrefab = template.radialVertical.gameObject;
            base.Build(template, parent);
        }
    }
}
