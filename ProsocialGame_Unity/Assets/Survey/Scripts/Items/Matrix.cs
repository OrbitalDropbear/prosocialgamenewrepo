﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SimpleSurvey
{
    public class Matrix : SurveyItem
    {
        public TextMeshProUGUI description;
        public RectTransform matrixHeader;
        public RectTransform matrixItem;

        private GameObject[] items;

        public override void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            base.Setup(slot, template);
            MatrixData data = (MatrixData)slot;

            // Set the matrix description.
            description.text = (SurveyData.nextQuestionNumber++) + ". " + data.description + RequiredExtra();

            // Build the header.
            GameObject temp = matrixHeader.GetComponentInChildren<TextMeshProUGUI>().gameObject;
            for (int i = 0; i < data.headers.Length; i++)
            {
                Instantiate(temp, matrixHeader).GetComponent<TextMeshProUGUI>().text = data.headers[i];
            }
            Destroy(temp);

            // Build each of the items.
            items = new GameObject[data.items.Length];
            for (int i = 0; i < data.items.Length; i++)
            {
                items[i] = Instantiate(matrixItem.gameObject, GetComponent<RectTransform>());

                //
                items[i].GetComponentInChildren<Image>(true).color = i % 2 == 0 ? template.evenHighlight : template.oddHighlight;

                // Assign the label for the item.
                items[i].GetComponentInChildren<TextMeshProUGUI>().text = data.items[i].item;
                temp = items[i].GetComponentInChildren<Toggle>().gameObject;

                // Create the correct number of checkboxes.
                for (int j = 0; j < data.headers.Length; j++)
                {
                    Instantiate(temp, temp.GetComponent<RectTransform>().parent);
                }
                Destroy(temp);
            }
            Destroy(matrixItem.gameObject);
        }

        //-----------------------------------------------------------------------------------------
        // 
        //-----------------------------------------------------------------------------------------
        public override void AddToResultsDictionary(Dictionary<string, string> resultsDictionary)
        {
            MatrixData data = (MatrixData)attachedData;
            for (int i = 0; i < data.items.Length; i++)
            {
                // Build the key.
                string key = data.items[i].itemKey.Length > 0 ? data.items[i].itemKey : data.items[i].item;

                // Build the value.
                string value = "";
                Toggle[] toggles = items[i].GetComponentsInChildren<Toggle>();
                for (int j = 0; j < toggles.Length; j++)
                {
                    if (toggles[j].isOn)
                    {
                        value += data.headers[j] + "|";                       
                    }
                }
                if (value.Length > 0) value = value.Remove(value.Length - 1);
                resultsDictionary.Add(key, value);
                Debug.Log(key + " : " + value);
            }
        }
    }
}