﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//-------------------------------------------------------------------------------------------------
// Stores all data for the horizontal radialsurvey item.
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Radio Item (Horizontal)", menuName = "Survey/Radio Item (Horizontal)", order = 100)]
    public class RadioHorizontalData : SurveyItemData
    {
        public string[] items;
        public bool noneOfTheAbove;
        public bool allOfTheAbove;
        public bool otherPleaseSpecify;

        //---------------------------------------------------------------------------------------------
        // Build this survey component with the specified template element.
        //---------------------------------------------------------------------------------------------
        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            slotPrefab = template.radialHorizontal.gameObject;
            base.Build(template, parent);
        }
    }
}
