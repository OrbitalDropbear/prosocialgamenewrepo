﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//-------------------------------------------------------------------------------------------------
// Stores all data for a survey item container (which contains a group of survey items).
//-------------------------------------------------------------------------------------------------
namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Container", menuName = "Survey/Container", order = 200)]
    public class ContainerData : SurveyItemData
    {
        public SurveyItemData[] surveySlots;
        public bool containerIsVisible = true;

        public override void Build(SurveyTemplate template, RectTransform parent)
        {
            // Build the container.
            Container obj = Instantiate(template.container, parent).GetComponent<Container>();
            obj.Setup(this, template);

            // Add each of the contained items into the container.
            for (int i = 0; i < surveySlots.Length; i++)
            {
                surveySlots[i].Build(template, obj.GetComponent<RectTransform>());
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(obj.GetComponent<RectTransform>());
        }
    }
}