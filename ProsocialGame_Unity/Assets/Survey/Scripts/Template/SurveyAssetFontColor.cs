﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleSurvey
{
    public class SurveyAssetFontColor : MonoBehaviour
    {
        public AssetFontColor assetFontColor;

        public enum AssetFontColor
        {
            LightDefault,
            LightAccent1,
            LightAccent2,
            DarkDefault,
            DarkAccent1,
            DarkAccent2
        };
    }
}
