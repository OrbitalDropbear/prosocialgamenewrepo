﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Survey Template", menuName = "Survey/Survey Template", order = 300)]
    public class SurveyTemplate : ScriptableObject
    {
        [Header("Colours")]
        public Color lightDefault;
        public Color lightAccent1;
        public Color lightAccent2;
        public Color darkDefault;
        public Color darkAccent1;
        public Color darkAccent2;

        [Header("Matrix Highlights")]
        public Color oddHighlight;
        public Color evenHighlight;

        [Header("Fonts")]
        public TMP_FontAsset fontStandard;
        public TMP_FontAsset fontHeader;
        public TMP_FontAsset fontMisc1;
        public TMP_FontAsset fontMisc2;

        [Header("Cursors")]
        public Texture2D hoverCursor;
        public Texture2D carotCursor;

        [Header("Survey Prefabs")]
        public GameObject survey;
        public GameObject surveyPage;
        public SurveyItem container;
        public SurveyItem checkbox;
        public SurveyItem radialHorizontal;
        public SurveyItem radialVertical;
        public SurveyItem radialVerticalImage;
        public SurveyItem inputSmall;
        public SurveyItem inputOneLine;
        public SurveyItem inputMultiLine;
        public SurveyItem dropdown;
        public SurveyItem matrix;
        public SurveyItem likertGroup;
        public SurveyItem rankOrder;
        public SurveyItem branchInput;
        
    }
}
