﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleSurvey
{
    public class SurveyAssetFont : MonoBehaviour
    {
        public AssetFont assetFont;

        public enum AssetFont
        {
            Standard,
            PageHeader,
            MiscFont1,
            MiscFont2
        };
    }
}
