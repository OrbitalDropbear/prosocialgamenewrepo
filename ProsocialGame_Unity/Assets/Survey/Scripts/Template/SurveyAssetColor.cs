﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleSurvey
{
    public class SurveyAssetColor : MonoBehaviour
    {
        public AssetColor assetColor;

        public enum AssetColor
        {
            LightDefault,
            LightAccent1,
            LightAccent2,
            DarkDefault,
            DarkAccent1,
            DarkAccent2
        };
    }
}
