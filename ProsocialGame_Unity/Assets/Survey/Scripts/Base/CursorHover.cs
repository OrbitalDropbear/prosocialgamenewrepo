﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace SimpleSurvey
{
    public class CursorHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public SurveyTemplate template;
        public CursorType cursor = CursorType.Hand;

        public enum CursorType
        {
            Hand,
            Carot
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (GetComponentInParent<Survey>() != null)
            {
                template = GetComponentInParent<Survey>().surveyData.surveyTemplate;
            }

            if (cursor == CursorType.Hand)
            {
                Texture2D tex = template.hoverCursor;
                Vector2 hotspot = new Vector2(tex.width / 3 - 1, 0);
                Cursor.SetCursor(tex, hotspot, CursorMode.Auto);
            }
            else if (cursor == CursorType.Carot)
            {
                Texture2D tex = template.carotCursor;
                Vector2 hotspot = new Vector2(tex.width / 4, 0);
                Cursor.SetCursor(tex, hotspot, CursorMode.Auto);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }

        public void OnDisable()
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }
}

