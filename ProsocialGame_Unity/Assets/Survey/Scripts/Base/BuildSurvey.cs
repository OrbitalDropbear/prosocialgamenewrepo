﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleSurvey
{
    public class BuildSurvey : MonoBehaviour
    {
        public SurveyData survey;

        private void Start()
        {
            Survey mySurvey = survey.BuildSurvey();
        }
    }
}
