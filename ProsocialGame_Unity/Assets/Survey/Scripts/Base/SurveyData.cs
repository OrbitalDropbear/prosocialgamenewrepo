﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

namespace SimpleSurvey
{
    [CreateAssetMenu(fileName = "New Survey", menuName = "Survey/Survey", order = 51)]
    public class SurveyData : ScriptableObject
    {

        public string surveyName;
        public SurveyTemplate surveyTemplate;
        public SurveyPageData[] surveyPages;
        public bool finalPageIsConfirmation = false;

        public static int nextQuestionNumber = 1;

        private Survey surveyObj;

        public SurveyPageData[] RandomisePages()
        {
            // Rebuild the array.
            SurveyPageData[] randomisedPages = new SurveyPageData[surveyPages.Length];
            for (int i = 0; i < surveyPages.Length; i++)
            {
                randomisedPages[i] = surveyPages[i];
            }

            // Randomise all elements which can be randomised.
            for (int i = 0; i < randomisedPages.Length; i++)
            {
                if (randomisedPages[i].randomise)
                {
                    while (true)
                    {
                        int r = Random.Range(0, randomisedPages.Length);
                        if (randomisedPages[r].randomise)
                        {
                            SurveyPageData temp = randomisedPages[r];
                            randomisedPages[r] = randomisedPages[i];
                            randomisedPages[i] = temp;
                            break;
                        }
                    }
                }
            }
            return randomisedPages;
        }

        public SurveyItemData[] RandomiseItems(SurveyItemData[] items)
        {
            // Rebuild the array.
            SurveyItemData[] randomisedItems = new SurveyItemData[items.Length];
            for (int i = 0; i < randomisedItems.Length; i++)
            {
                randomisedItems[i] = items[i];
            }

            // Randomise all elements which can be randomised.
            for (int i = 0; i < randomisedItems.Length; i++)
            {
                if (randomisedItems[i].randomise)
                {
                    while (true)
                    {
                        int r = Random.Range(0, randomisedItems.Length);
                        if (randomisedItems[r].randomise)
                        {
                            SurveyItemData temp = randomisedItems[r];
                            randomisedItems[r] = randomisedItems[i];
                            randomisedItems[i] = temp;
                            break;
                        }
                    }
                }
            }
            return randomisedItems;
        }

        public Survey BuildSurvey()
        {
            nextQuestionNumber = 1;

            // Create the default survey object.
            surveyObj = Instantiate(surveyTemplate.survey).GetComponent<Survey>();
            surveyObj.surveyPageObjs = new SurveyPage[surveyPages.Length];
            surveyObj.surveyData = this;
            surveyObj.name = surveyName;

            // Create each of the survey pages.
            SurveyPageData[] randomisedPages = RandomisePages();
            for (int i = 0; i < randomisedPages.Length; i++)
            {
                // Create the survey page.
                SurveyPage surveyPageObj = Instantiate(surveyTemplate.surveyPage, surveyObj.rectTrans).GetComponent<SurveyPage>();
                surveyObj.surveyPageObjs[i] = surveyPageObj;
                surveyPageObj.surveyObj = surveyObj;
                

                // Build the survey page.
                //surveyPages[i].Build(surveyTemplate, surveyObj.GetComponent<RectTransform>());
                surveyPageObj.Setup(randomisedPages[i], i, finalPageIsConfirmation ? randomisedPages.Length - 1 : randomisedPages.Length);

                // Build each of the survey items.
                SurveyItemData[] randomiseditems = RandomiseItems(randomisedPages[i].surveySlots);
                for (int j = 0; j < randomiseditems.Length; j++)
                {

                    randomiseditems[j].Build(surveyTemplate, surveyPageObj.contentContainer);
                }

                // Rebuild the layout.
                LayoutRebuilder.ForceRebuildLayoutImmediate(surveyPageObj.contentContainer);
                surveyPageObj.gameObject.SetActive(i == 0);
            }

            
            // Adjust all image component colours with a relevant tag.
            SurveyAssetColor[] customColors = surveyObj.GetComponentsInChildren<SurveyAssetColor>(true);
            foreach (SurveyAssetColor customColor in customColors)
            {
                Image image = customColor.GetComponent<Image>();
                SurveyAssetColor.AssetColor color = customColor.assetColor;
                if (image != null)
                {
                    switch (color)
                    {
                        case SurveyAssetColor.AssetColor.DarkDefault:
                            image.color = surveyTemplate.darkDefault;
                            break;
                        case SurveyAssetColor.AssetColor.DarkAccent1:
                            image.color = surveyTemplate.darkAccent1;
                            break;
                        case SurveyAssetColor.AssetColor.DarkAccent2:
                            image.color = surveyTemplate.darkAccent2;
                            break;
                        case SurveyAssetColor.AssetColor.LightDefault:
                            image.color = surveyTemplate.lightDefault;
                            break;
                        case SurveyAssetColor.AssetColor.LightAccent1:
                            image.color = surveyTemplate.lightAccent1;
                            break;
                        case SurveyAssetColor.AssetColor.LightAccent2:
                            image.color = surveyTemplate.lightAccent2;
                            break;
                    }
                }
            }

            // Adjust all text component colours with a relevant tag.
            SurveyAssetFontColor[] customFontColors = surveyObj.GetComponentsInChildren<SurveyAssetFontColor>(true);
            foreach (SurveyAssetFontColor customColor in customFontColors)
            {
                TextMeshProUGUI text = customColor.GetComponent<TextMeshProUGUI>();
                SurveyAssetFontColor.AssetFontColor color = customColor.assetFontColor;
                if (text != null)
                {
                    switch (color)
                    {
                        case SurveyAssetFontColor.AssetFontColor.DarkDefault:
                            text.color = surveyTemplate.darkDefault;
                            break;
                        case SurveyAssetFontColor.AssetFontColor.DarkAccent1:
                            text.color = surveyTemplate.darkAccent1;
                            break;
                        case SurveyAssetFontColor.AssetFontColor.DarkAccent2:
                            text.color = surveyTemplate.darkAccent2;
                            break;
                        case SurveyAssetFontColor.AssetFontColor.LightDefault:
                            text.color = surveyTemplate.lightDefault;
                            break;
                        case SurveyAssetFontColor.AssetFontColor.LightAccent1:
                            text.color = surveyTemplate.lightAccent1;
                            break;
                        case SurveyAssetFontColor.AssetFontColor.LightAccent2:
                            text.color = surveyTemplate.lightAccent2;
                            break;
                    }
                }
            }

            // Adjust all text component fonts with a relevant tag.
            SurveyAssetFont[] customFonts = surveyObj.GetComponentsInChildren<SurveyAssetFont>(true);
            foreach (SurveyAssetFont customFont in customFonts)
            {
                TextMeshProUGUI text = customFont.GetComponent<TextMeshProUGUI>();
                SurveyAssetFont.AssetFont font = customFont.assetFont;
                if (text != null)
                {
                    switch (font)
                    {
                        case SurveyAssetFont.AssetFont.Standard:
                            text.font = surveyTemplate.fontStandard;
                            break;
                        case SurveyAssetFont.AssetFont.PageHeader:
                            text.font = surveyTemplate.fontHeader;
                            break;
                        case SurveyAssetFont.AssetFont.MiscFont1:
                            text.font = surveyTemplate.fontMisc1;
                            break;
                        case SurveyAssetFont.AssetFont.MiscFont2:
                            text.font = surveyTemplate.fontMisc2;
                            break;
                    }
                }
            }
            return surveyObj;
        }

        public void Show()
        {
            surveyObj.gameObject.SetActive(true);
        }
    }
}