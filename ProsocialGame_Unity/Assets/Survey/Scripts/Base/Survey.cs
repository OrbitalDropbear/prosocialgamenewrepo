﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace SimpleSurvey
{
    public class Survey : MonoBehaviour
    {
        public class SurveySubmitEvent : UnityEvent<SurveyKeyValuePair[]> { }

        public class SurveyKeyValuePair
        {
            public string key;
            public string value;
            public SurveyKeyValuePair (string key, string value)
            {
                this.key = key;
                this.value = value;
            }
        }


        public SurveySubmitEvent unityEvent;

        public void AddEventListener (UnityAction<SurveyKeyValuePair[]> call)
        {
            if (unityEvent == null) unityEvent = new SurveySubmitEvent();
            unityEvent.AddListener(call);
        }

        public void RemoveEventListener (UnityAction<SurveyKeyValuePair[]> call)
        {
            if (unityEvent == null) unityEvent = new SurveySubmitEvent();
            unityEvent.RemoveListener(call);
        }

        public RectTransform rectTrans;

        public SurveyPage[] surveyPageObjs;

        int currentPage = 0;

        [HideInInspector]
        public SurveyData surveyData;

        private bool submitted = false;

        //---------------------------------------------------------------------------------------------
        // Take the user to the next page.
        //---------------------------------------------------------------------------------------------
        public void NextPage()
        {
            SurveyItem[] surveyItems = surveyPageObjs[currentPage].GetComponentsInChildren<SurveyItem>();
            foreach (SurveyItem surveyItem in surveyItems)
            {
                if (!surveyItem.IsValidSubmission()) {
                    surveyPageObjs[currentPage].errorWarning.GetComponent<Animator>().SetTrigger("Error");
                    return;
                }
            }

            if (currentPage == surveyPageObjs.Length - 1)
            {
                gameObject.SetActive(false);
            }

            if ((currentPage == surveyPageObjs.Length - 1 && !surveyData.finalPageIsConfirmation) ||
                (currentPage == surveyPageObjs.Length - 2 && surveyData.finalPageIsConfirmation))
            {
                SubmitSurvey();
            }

            if (currentPage != surveyPageObjs.Length - 1)
            {
                currentPage++;
                for (int i = 0; i < surveyPageObjs.Length; i++)
                {
                    surveyPageObjs[i].gameObject.SetActive(i == currentPage);
                }
            }
        }
        
        //---------------------------------------------------------------------------------------------
        // Take the user back to the previous page.
        //---------------------------------------------------------------------------------------------
        public void PreviousPage()
        {
            if (currentPage != 0)
            {
                currentPage--;
                for (int i = 0; i < surveyPageObjs.Length; i++)
                {
                    surveyPageObjs[i].gameObject.SetActive(i == currentPage);
                }
            }
        }

        private void Update()
        {

        }

        //---------------------------------------------------------------------------------------------
        // Submit the survey, storing the results to a file.
        //---------------------------------------------------------------------------------------------
        public void SubmitSurvey ()
        {
            if (!submitted)
            {
                submitted = true;

                // Build a list of all keys used in the survey.
                List<string> keys = new List<string>();
                foreach (SurveyPageData pageData in  surveyData.surveyPages)
                {
                    foreach (SurveyItemData itemData in pageData.surveySlots)
                    {
                        itemData.AddToKeyList(keys);
                    }
                }
                
                // Assign each value to a dictionary.
                Dictionary<string, string> surveyDictionary = new Dictionary<string, string>();
                SurveyItem[] surveyItems = GetComponentsInChildren<SurveyItem>(true);
                foreach (SurveyItem surveyItem in surveyItems)
                {
                    surveyItem.AddToResultsDictionary(surveyDictionary);
                }

                // Generate an ordered array of key/value pairs.
                SurveyKeyValuePair[] surveyKeyValuePairs = new SurveyKeyValuePair[keys.Count];
                for (int i = 0; i < surveyKeyValuePairs.Length; i++)
                {
                    surveyKeyValuePairs[i] = new SurveyKeyValuePair(keys[i], surveyDictionary[keys[i]]);
                }
                
                if (!File.Exists("Survey.csv"))
                {
                    string line = "";
                    for (int i = 0; i < keys.Count; i++)
                    {
                        line += ProcessCSVOutput(keys[i]) + ",";
                    }
                    File.WriteAllText("Survey.csv", line + "\n");
                }
                if (File.Exists("Survey.csv"))
                {
                    string line = "";
                    for (int i = 0; i < keys.Count; i++)
                    {
                        line += ProcessCSVOutput(surveyDictionary[keys[i]]) + ",";
                    }
                    File.AppendAllText("Survey.csv", line + "\n");
                }

                if (unityEvent != null)
                {
                    unityEvent.Invoke(surveyKeyValuePairs);
                }
            } 
        }

        private string ProcessCSVOutput (string data)
        {
            if (data.Contains("\""))
            {
                data = data.Replace("\"", "\"\"");
            }

            if (data.Contains(","))
            {
                data = String.Format("\"{0}\"", data);
            }

            data = data.Replace("\n", "");
            data = data.Replace("\r", "");
            data = data.Replace("\r\n", "");
            data = data.Replace("<br>", "");

            if (data.Contains(System.Environment.NewLine))
            {
                //data = data.Replace(System.Environment.NewLine, "");
                data = data.Replace("\n", "");
                data = data.Replace("\r", "");
                data = data.Replace("\r\n", "");
                data = data.Replace("<br>", "");
                //data = String.Format("\"{0}\"", data);
            }
            return data;
        }
    }
}


