﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleSurvey
{
    public class SurveyItemData : ScriptableObject
    {
        public string description;
        protected GameObject slotPrefab;

        public string itemReferenceKey;

        public bool randomise = false;
        public bool required = true;

        public virtual void Build(SurveyTemplate template, RectTransform parent)
        {
            SurveyItem obj = Instantiate(slotPrefab, parent).GetComponent<SurveyItem>();
            obj.Setup(this, template);
        }

        public virtual void AddToKeyList (List<string> keys)
        {
            keys.Add(itemReferenceKey.Length > 0 ? itemReferenceKey : description);
        }
    }
}
