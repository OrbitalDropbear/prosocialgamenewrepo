﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleSurvey
{
    public class SurveyItem : MonoBehaviour
    {
        [HideInInspector] public SurveyItemData attachedData;

        public virtual void Setup(SurveyItemData slot, SurveyTemplate template)
        {
            attachedData = slot;
        }

        //-----------------------------------------------------------------------------------------
        // Returns true if the value for this item is appropriate to submit, otherwise false.
        //-----------------------------------------------------------------------------------------
        public virtual void AddToResultsDictionary(Dictionary<string, string> resultsDictionary)
        {
            if (!resultsDictionary.ContainsKey (GetKey()))
            {
                resultsDictionary.Add(GetKey(), GetValue());
                //Debug.Log(GetKey() + " : " + GetValue());
            }
            else
            {
                Debug.Log(name + " - Survey key \"" + GetKey() + "\" already exists. Keys must be unique.");
            }
        }

        //-----------------------------------------------------------------------------------------
        // Returns true if the value for this item is appropriate to submit, otherwise false.
        //-----------------------------------------------------------------------------------------
        public virtual bool IsValidSubmission ()
        {
            return false;
        }

        //-----------------------------------------------------------------------------------------
        // Returns the current submission value for this item. String format for writing to disc.
        //-----------------------------------------------------------------------------------------
        public virtual string GetValue ()
        {
            return "";
        }

        //-----------------------------------------------------------------------------------------
        // Returns the unique key for this item.
        //-----------------------------------------------------------------------------------------
        public virtual string GetKey ()
        {
            if (attachedData.itemReferenceKey.Length > 0)
            {
                return attachedData.itemReferenceKey;
            }
            else
            {
                return attachedData.description;
            }
        }

        /*
        public virtual bool IsFilled()
        {
            return false;
        }

        public virtual bool IsFilledCorrectly()
        {
            return false;
        }
        */

        public string RequiredExtra ()
        {
            if (attachedData.required)
            {
                return "";// return " <color=red>*</color>";
            }
            return "";
        }
    }
}

