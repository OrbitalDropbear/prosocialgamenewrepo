using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Demo : MonoBehaviour
{
    public static string completionCode;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DetermineCondition());
    }

    /// <summary>
    /// Assign the user to the condition which currently has the least participants.
    /// </summary>
    IEnumerator DetermineCondition()
    {
        completionCode = GenerateCompletionCode();
        float EXPECTED_STUDY_LENGTH = 1200; // 20 minutes.

        Debug.Log("Requesting condition...");

        UnityWebRequest www = UnityWebRequest.Get($"https://cinchdb.com/o3RL16FqbaEZiKxrUf4PMIgy52Wkt9/retrieve/csv");
        yield return www.SendWebRequest();
        string[] records = www.downloadHandler.text.Split('\n');

        int[] conditions = new int[3];
        foreach (string record in records)
        {
            string[] parts = record.Split(',');
            if (parts.Length > 4)
            {
                // If the user has finished the study, count them in the total samples for their condition.
                if (parts[3] == "1" || (int.Parse(parts[1]) + EXPECTED_STUDY_LENGTH > timeSinceEpoch()))
                {
                    conditions[int.Parse(parts[2])]++;
                }
            }
        }

        // Find the condition with the minimum participants.
        int conditionToAssign = 0;
        for (int i = 0; i < conditions.Length; i++)
        {
            if (conditions[i] < conditions[conditionToAssign])
            {
                conditionToAssign = i;
            }
        }

        Debug.Log($"Condition {conditionToAssign} given.");
        yield return StartCoroutine(AddNewStartRecord(conditionToAssign));
        Debug.Log($"Condition {conditionToAssign} started.");
        // PUT YOUR CONDITION SPECIFIC CODE HERE ... conditionToAssign will be 0, 1 or 2.
    }

    public string GenerateCompletionCode()
    {
        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        string buffer = "";
        for (int i = 0; i < 5; i++)
        {
            buffer += chars[Random.Range(0, chars.Length)];
        }
        return buffer;
    }

    /// <summary>
    /// Tells the system that a user has *started* the study with the given condition number.
    /// </summary>
    IEnumerator AddNewStartRecord(int condition)
    {
        string time = timeSinceEpoch().ToString();
        UnityWebRequest www = UnityWebRequest.Get($"https://cinchdb.com/o3RL16FqbaEZiKxrUf4PMIgy52Wkt9/insert/{completionCode}/{time}/{condition}");
        yield return www.SendWebRequest();
    }

    /// <summary>
    /// Tells the system that a user has *finished* the studyr.
    /// </summary>
    IEnumerator AddNewStopRecord(int condition)
    {
        UnityWebRequest www = UnityWebRequest.Get($"https://cinchdb.com/o3RL16FqbaEZiKxrUf4PMIgy52Wkt9/update/col1={completionCode}/col4=1");
        yield return www.SendWebRequest();
    }

    /// <summary>
    /// Return the time in seconds since the UNIX epoch.
    /// </summary>
    public static int timeSinceEpoch()
    {
        System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        int cur_time = (int)(System.DateTime.UtcNow - epochStart).TotalSeconds;
        return cur_time;
    }
}
