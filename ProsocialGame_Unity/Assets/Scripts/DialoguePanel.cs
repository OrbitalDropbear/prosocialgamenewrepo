﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DialoguePanel : MonoBehaviour
{
    [Header("Panel Moving")]
    public RectTransform onScreenTarget;
    public RectTransform offScreenTarget;
    public float secondsToTarget;

    [Header("Dialogue")]
    public TextMeshProUGUI dialogueText;
    public bool onScreen;
    private float moveDist;
    private float speed;
    private Vector3 startPos;
    private Vector3 endPos;
    private int maxCharLength;


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<RectTransform>().position = offScreenTarget.position;
        onScreen = false;
    }

    public void Move(){

        if(onScreen){
            startPos = GetComponent<RectTransform>().position;
            endPos = offScreenTarget.position;
        } else {
            startPos = GetComponent<RectTransform>().position;
            endPos = onScreenTarget.position;
        }

        moveDist = 0;

        onScreen = !onScreen;

        StartCoroutine("Moving");
        
    }


    public void DisplayDialogue (string dialogue){
        dialogueText.text = dialogue;
    }

    private IEnumerator Moving(){

        while(GetComponent<RectTransform>().position != endPos){
            speed = Vector3.Distance(startPos, endPos) / secondsToTarget;
            moveDist += speed * Time.deltaTime;
            float interpolationnValue = EasingFunctions.EaseOutElastic(Mathf.Clamp01(moveDist / Vector3.Distance(startPos, endPos)));
            GetComponent<RectTransform>().position = Vector3.LerpUnclamped(startPos, endPos, interpolationnValue);
            yield return null;
        }

    }
}
