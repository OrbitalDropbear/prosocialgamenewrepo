﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternTask : MonoBehaviour
{
    public Material offMaterial;
    public Material onMaterial;
    
    public List<Lantern> lanterns;

    ParticleSystem pOI;
    bool inRange;
    ScaleButtonPrompt prompt;

    int validLanterns = 0;

    bool interacted = false;

    LevelManager_Beach levelManager;

    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
        pOI = GetComponentInChildren<ParticleSystem>();
        levelManager = FindObjectOfType<LevelManager_Beach>();
        foreach(Lantern l in lanterns){
            l.enabled = false;
        }
    }

    private void Update() {
        if(inRange && Input.GetButtonDown("Interact") && !interacted){
            interacted = true;
            pOI.gameObject.SetActive(false);
            foreach(Lantern l in lanterns){
                l.enabled = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            inRange = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
        }
    }

    public void ActivateLantern(bool valid){
        if(valid){
            validLanterns++;
        } else {
            validLanterns = 0;
            StartCoroutine("FlashLanterns");
        }

        if(validLanterns >= 4){
            foreach(Lantern l in lanterns){
                l.enabled = false;
            }
            levelManager.ActivateTV(onMaterial);
        }
    }

    IEnumerator FlashLanterns(){
        int flashTimes = 3;
        
        for(int i = 0; i <= flashTimes; i++){
            foreach(Lantern l in lanterns){
                Material[] mats = l.GetComponent<Renderer>().materials;
                mats[1] = onMaterial;
                l.GetComponent<Renderer>().materials = mats;
            }

            yield return new WaitForSeconds(.1f);

            foreach(Lantern l in lanterns){
                Material[] mats = l.GetComponent<Renderer>().materials;
                mats[1] = offMaterial;
                l.GetComponent<Renderer>().materials = mats;
            }
            yield return new WaitForSeconds(.1f);
        }

        foreach(Lantern l in lanterns){
            l.ResetLantern();
        }
    }
}
