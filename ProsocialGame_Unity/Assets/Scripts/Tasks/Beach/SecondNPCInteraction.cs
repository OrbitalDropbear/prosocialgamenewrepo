﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondNPCInteraction : MonoBehaviour
{
    LevelManager_GasStation levelManager;
    
    bool inRange = false;
    bool interacted = false;
    public bool activateBigGun = false;
    bool givenWater = false;
    ScaleButtonPrompt prompt;
    TalkingCamZone talkingCamZone;
    public Animator npcAnimator;
        
    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
        levelManager = FindObjectOfType<LevelManager_GasStation>();
        talkingCamZone = GetComponent<TalkingCamZone>();
        talkingCamZone.enabled = false;
    }


    private void Update() {
        if(inRange && Input.GetButtonDown("Interact") && !interacted && givenWater){
            interacted = true;
            prompt.HidePrompt();
            GetComponentInChildren<ParticleSystem>().Stop();
            if(activateBigGun){
                levelManager.ActivateGasCan();
            } else {
                levelManager.ActivateGasCanWithoutGun();
            }
            levelManager.surveyScript.RecordSecondTalkingStart();
            this.enabled = false;
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && !interacted && this.enabled){
            inRange = true;
            if(givenWater){
                prompt.ShowPrompt();
            }
        }

        if(other.CompareTag("Bottle") && this.enabled){
            other.GetComponent<PickupableObject>().DropObject();
            other.GetComponent<PickupableObject>().enabled = false;
            Destroy(other.gameObject);
            givenWater = true;
            talkingCamZone.enabled = true;
            npcAnimator.SetTrigger("Lift");
            levelManager.surveyScript.RecordHelpingEnd();         
        }
    }

    private void OnTriggerStay(Collider other) {
        if(other.CompareTag("Player") && !interacted && this.enabled && givenWater){
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
            prompt.HidePrompt();
        }        
    }
}
