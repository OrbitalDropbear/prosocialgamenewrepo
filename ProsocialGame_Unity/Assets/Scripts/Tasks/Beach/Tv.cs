﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tv : MonoBehaviour
{
    

    LevelManager_Beach levelManager;
    
    bool inRange = false;
    bool interacted = false;
    ScaleButtonPrompt prompt;




    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
        levelManager = FindObjectOfType<LevelManager_Beach>();
    }


    private void Update() {
        if(inRange && Input.GetButtonDown("Interact") && !interacted){
            levelManager.SpawnNPC();
            interacted = true;
            prompt.HidePrompt();
            GetComponentInChildren<ParticleSystem>().Stop();
            this.enabled = false;
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && !interacted && this.enabled){
            inRange = true;
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
            prompt.HidePrompt();
        }        
    }
}
