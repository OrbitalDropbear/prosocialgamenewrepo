﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCInteraction : MonoBehaviour
{
    //LevelManager_Beach levelManager;
    
    bool inRange = false;
    bool interacted = false;
    ScaleButtonPrompt prompt;

    public Animator npcAnimator;
    float timer = 0;
    float randomTime = 15;

    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
        //levelManager = FindObjectOfType<LevelManager_Beach>();
    }


    private void Update() {
        if(inRange && Input.GetButtonDown("Interact") && !interacted){
            interacted = true;
            prompt.HidePrompt();
            GetComponentInChildren<ParticleSystem>().Stop();
            this.enabled = false;
        }

        if(timer > randomTime){
            npcAnimator.SetTrigger("Lift");
            timer = 0;
            randomTime = Random.Range(5, 15);
        }

        timer += Time.deltaTime;
         
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && !interacted && this.enabled){
            inRange = true;
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
            prompt.HidePrompt();
        }        
    }
}
