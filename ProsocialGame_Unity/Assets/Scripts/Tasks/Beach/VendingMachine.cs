﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendingMachine : MonoBehaviour
{

    public GameObject bottle;
    //public GameObject dangerPuddle;
    public GameObject explosionOutcome;
    public ParticleSystem explosion;
    LevelManager_GasStation levelManager;
    GameManager gameManager;
    bool inRange = false;
    bool interacted = false;

    PlayerHealth playerHealth;
    
    ScaleButtonPrompt[] prompts;


    private void Awake() {
        prompts = GetComponentsInChildren<ScaleButtonPrompt>();
        levelManager = FindObjectOfType<LevelManager_GasStation>();
        gameManager = FindObjectOfType<GameManager>();
        playerHealth = FindObjectOfType<PlayerHealth>();
        if(gameManager.GetVendingMachine()){
            explosionOutcome.SetActive(true);
            //dangerPuddle.SetActive(false);
            this.enabled = false;
            this.gameObject.SetActive(false);
        } else if (gameManager.GetDecisionMade()){
            this.enabled = false;
            this.gameObject.SetActive(false); 
        }

    }


    private void Update() {
        if(inRange && Input.GetButtonDown("Interact") && !interacted){
            interacted = true;
            foreach(ScaleButtonPrompt prompt in prompts){
                prompt.HidePrompt();
            }
            GetComponentInChildren<ParticleSystem>().Stop();
            Instantiate(bottle, transform.position, transform.rotation);
            levelManager.ActivateSecondNPCInteraction();
            explosionOutcome.SetActive(true);
            explosion.Play();
            //dangerPuddle.SetActive(false);
            playerHealth.HealthSacrificeVisualUpdate();
            gameManager.SetVendingMachine();
            this.enabled = false;
            this.gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && !interacted && this.enabled){
            inRange = true;
            foreach(ScaleButtonPrompt prompt in prompts){
                prompt.ShowPrompt();
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
            foreach(ScaleButtonPrompt prompt in prompts){
                prompt.HidePrompt();
            }
        }        
    }
}
