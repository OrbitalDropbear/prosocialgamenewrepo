﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager_Beach : MonoBehaviour
{
    
    public GameObject tv;
    public GameObject tvInteractionZone;
    public GameObject npc;
    public GameObject npcInteractionZone;
    public GameObject vendingMachineInteraction;
    public GameObject npcInteractionZone2;
    public GameObject carInteractionZone;
    public ParticleSystem dispenseCoins;

    GameManager gameManager;
    CustomCharacterController player;
    bool vendingMachineActivated = false;

    private SurveyControllerExample surveyScript;

    private void Awake() {
        gameManager = FindObjectOfType<GameManager>();
        player = FindObjectOfType<CustomCharacterController>();
        surveyScript = FindObjectOfType<SurveyControllerExample>();
        Camera.main.ResetCullingMatrix();
        surveyScript.RecordDataTime("BeachStart");
    }

    public void ActivateTV(Material onMaterial){
        Material[] mats = tv.GetComponent<Renderer>().materials;
        mats[1] = onMaterial;
        tv.GetComponent<Renderer>().materials = mats;
        tvInteractionZone.SetActive(true);
    }

    public void SpawnNPC(){
        npc.SetActive(true);
        npcInteractionZone.SetActive(true);
    }

    public void ActivateVendingMachines(){
        if(!vendingMachineActivated){
            vendingMachineInteraction.SetActive(true);
            vendingMachineActivated = true;
        }
    }

    public void ActivateSecondNPCInteraction(){
        if(gameManager){
            gameManager.SetCoinTotal(-40);
        }
        dispenseCoins.Play();
        npcInteractionZone2.SetActive(true);
    }

    public void ActivateCar(){
        carInteractionZone.SetActive(true);
        gameManager.SetBigGunStatus();
    }

    public void ActivateCarWithoutGun(){
        carInteractionZone.SetActive(true);
    }

    public void EndLevel(){
        StartCoroutine("EndOfGameSequence");
    }

    private IEnumerator EndOfGameSequence(){
        player.speed = 0;
        if(gameManager){
            yield return gameManager.fadeScreen();
            gameManager.NextLevel(Mathf.Abs(3-surveyScript.GetRandomCondition()));
        }   
    }
}
