﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestTalkingZone : TalkingCamZone
{
    public GameObject choiceButtons;
    public GameObject interactionPrompt;
    LevelManager_GasStation levelManager;
    CustomCharacterController player;
    bool questChoice = false;
    bool decisionMade = false;
    float playerBaseSpeed;

    private void Awake() {
        levelManager = FindObjectOfType<LevelManager_GasStation>();
        player = FindObjectOfType<CustomCharacterController>();
        playerBaseSpeed = player.speed;
    }
    protected override IEnumerator PassDialogue (string dialogue){
        levelManager.surveyScript.RecordFirstTalkingStart();
        levelManager.surveyScript.RecordDataTime("FirstInteractionStart x " + timesInteracted);
        player.speed = 0;
        choiceButtons.SetActive(false);
        interactionPrompt.SetActive(true);
        questChoice = false;
        string[] sentences = dialogue.Split(';');
        int index = 0;
        yield return null;
        while(index < sentences.Length || !dialoguePanel.onScreen){
            if(sentences[index].Contains("^")){
                choiceButtons.SetActive(true);
                interactionPrompt.SetActive(false);
                sentences[index] = sentences[index].TrimStart('^');
                questChoice = true;
            }
            if(sentences[index].Contains("`")){
                npcName.SetActive(true);
                sentences[index] = sentences[index].TrimStart('`');
            }
            dialoguePanel.DisplayDialogue(sentences[index]);
            if(Input.GetButtonDown("Interact") && !questChoice){                
                index++;
            }
            if(QuestStatus()){
                index++;
            }
            yield return null;
        }
        dialoguePanel.Move();
        base.talking = false;
        base.cam.gameObject.SetActive(false);
        player.speed = playerBaseSpeed;
        choiceButtons.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        npcName.SetActive(false);
        interactionPrompt.SetActive(true);
        Destroy(gameObject);
    }

    public void AcceptQuest(){
        levelManager.ActivateVendingMachines();
        levelManager.surveyScript.RecordAppendData("HelpDecision", "Helped");
        levelManager.surveyScript.RecordFirstTalkingEnd();
        questChoice = true;
        UpdateQuestStatus(true);
    }

    public void DeclineQuest(){
        levelManager.ActivateGasCanWithoutGun();
        levelManager.surveyScript.RecordAppendData("HelpDecision", "No Help");
        levelManager.surveyScript.RecordFirstTalkingEnd();
        questChoice = true;
        UpdateQuestStatus(true);
    }

    void UpdateQuestStatus(bool status){
        decisionMade = status;
        Cursor.lockState = CursorLockMode.Locked;
    }

    bool QuestStatus(){
        return decisionMade;
    }

}
