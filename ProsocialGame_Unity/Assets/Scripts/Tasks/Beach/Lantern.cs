﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lantern : MonoBehaviour
{
    public LanternTask lanternTask;
    public Material onMaterial;
    public bool valid;
    bool inRange = false;
    bool interacted = false;
    ScaleButtonPrompt prompt;


    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
    }

    private void Update() {
        if(inRange && Input.GetButtonDown("Interact") && !interacted){
            Material[] mats = GetComponent<Renderer>().materials;
            mats[1] = onMaterial;
            GetComponent<Renderer>().materials = mats;
            interacted = true;
            lanternTask.ActivateLantern(valid);
            prompt.HidePrompt();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && !interacted && this.enabled){
            inRange = true;
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
            prompt.HidePrompt();
        }        
    }

    public void ResetLantern(){
        interacted = false;
    }


}
