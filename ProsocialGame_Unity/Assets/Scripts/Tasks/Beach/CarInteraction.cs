﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarInteraction : MonoBehaviour
{
    
    LevelManager_Beach levelManager;
    
    bool inRange = false;
    ScaleButtonPrompt prompt;

    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
        levelManager = FindObjectOfType<LevelManager_Beach>();
    }


    private void Update() {
        if(inRange && Input.GetButtonDown("Interact")){
            prompt.HidePrompt();
            GetComponentInChildren<ParticleSystem>().Stop();
            levelManager.EndLevel();
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && this.enabled){
            inRange = true;
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
            prompt.HidePrompt();
        }        
    }
}
