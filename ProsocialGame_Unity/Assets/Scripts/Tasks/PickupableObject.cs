﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupableObject : MonoBehaviour
{

    CustomCharacterController player;
    ScaleButtonPrompt prompt;
    bool held = false;
    bool falling = true;
    bool interacted = false;
    Rigidbody rb;
    public Vector3 rotationAmount;
    public Vector3 offsetAmount;
    bool inRange = false;

    private void Awake()
    {
        player = FindObjectOfType<CustomCharacterController>();
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if(held){
            prompt.HidePrompt();
            if(offsetAmount == Vector3.zero){
                transform.localPosition = player.objectHoldOffset;
            } else {
                transform.localPosition = offsetAmount;
            }
            
        }

        if(held && (player.gunDrawn || (Input.GetButtonDown("Interact") && interacted == false))){
            DropObject();
            interacted = true;
        }

        if(inRange && Input.GetButtonDown("Interact") && !held && !interacted){
            held = true;
            interacted = true;
            rb.isKinematic = true;
            transform.rotation = Quaternion.Euler(rotationAmount);
            GetComponent<Collider>().enabled = false;
            gameObject.transform.SetParent(player.gameObject.transform);
            player.HoldObject(true);
            prompt.HidePrompt();

            FuelCanister canister = GetComponent<FuelCanister>();
            if(canister){
                canister.SwitchObjectives(true);
            }
        } 

        interacted = false;

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && this.enabled == true){
            inRange = true;
            prompt.ShowPrompt();    
        }
    }

    private void OnTriggerStay(Collider other) {
        if(other.CompareTag("Player") && this.enabled == true){
            inRange = true;    
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player") && this.enabled == true){
            inRange = false; 
            prompt.HidePrompt();   
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(falling && !other.gameObject.CompareTag("Player")){
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
    }

    public void DropObject(){
        held = false;
        rb.constraints = RigidbodyConstraints.None;
        rb.isKinematic = false;
        falling = true;
        gameObject.transform.SetParent(null);
        GetComponent<Collider>().enabled = true;
        player.HoldObject(false);

        FuelCanister canister = GetComponent<FuelCanister>();
        if(canister){
            canister.SwitchObjectives(false);
        }
    }
}
