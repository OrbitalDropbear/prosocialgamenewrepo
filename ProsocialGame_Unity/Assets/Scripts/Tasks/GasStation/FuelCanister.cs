﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuelCanister : MonoBehaviour
{
    PickupableObject pickup;
    Image ui_Timer;
    public LevelManager_GasStation levelManager;

    float timer;
    public float maxTime;
    public GameObject zombieHorde;
    public GameObject zombieHorde2;
    public GameObject zombieHorde3;

    public GameObject spawnZone;
    public GameObject spawnZone2;

    public float spawnTimer1;
    public float spawnTimer2;
    public float spawnTimer3;

    public ParticleSystem carInterestPoint;

    private void Awake() {
        pickup = GetComponent<PickupableObject>();
        ui_Timer = GetComponentInChildren<Image>();
        pickup.enabled = false;
        
    }

    private void OnEnable() {
        levelManager.surveyScript.RecordDataTime("HordeStart");
        levelManager.surveyScript.RecordHordeStart();
        StartCoroutine("SpawnHordes");
    }

    private void Update() {
        timer += Time.deltaTime;
        ui_Timer.fillAmount = timer/maxTime;
        if(timer >= maxTime && !pickup.enabled){
            pickup.enabled = true;
            Image[] images = GetComponentsInChildren<Image>();
            foreach(Image image in images){
               image.gameObject.SetActive(false);
            }
            SwitchObjectives(false);
            carInterestPoint.gameObject.SetActive(true);
        }
    }

    IEnumerator SpawnHordes(){
        bool spawn1 = false;
        bool spawn2 = false;
        bool spawn3 = false;

        while(timer < maxTime){
            if(timer > maxTime * spawnTimer1 && !spawn1){
                zombieHorde.SetActive(true);
                spawn1 = true; 
            }

            if(timer > maxTime * spawnTimer2 && !spawn2){
                zombieHorde2.SetActive(true);
                spawn2 = true;                
            }

            if(timer > maxTime * spawnTimer3 && !spawn3){
                zombieHorde3.SetActive(true);
                spawn3 = true;                
            }
            yield return null;
        }
        
        spawnZone.SetActive(true);
        spawnZone2.SetActive(true);
    }

    public void SwitchObjectives(bool goToCar){
        if(goToCar){
            levelManager.UpdateObjectivesListCar();
        } else {
            levelManager.UpdateObjectivesListGasComplete();
        }
    }
}
