﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasCanInteraction : MonoBehaviour
{
    public GameObject fuelCan;
    public int fuelCost = -50;
    LevelManager_GasStation levelManager;
    GameManager gameManager;
    bool inRange = false;
    bool interacted = false;
    ScaleButtonPrompt prompt;

    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
        levelManager = FindObjectOfType<LevelManager_GasStation>();
        gameManager = FindObjectOfType<GameManager>();
    }


    private void Update() {
        if(inRange && Input.GetButtonDown("Interact") && !interacted){
            interacted = true;
            prompt.HidePrompt();
            GetComponentInChildren<ParticleSystem>().Stop();
            if(gameManager){
                gameManager.SetCoinTotal(fuelCost);
            }
            fuelCan.SetActive(true);
            gameObject.SetActive(false);
            levelManager.UpdateObjectivesListHorde();
            levelManager.ShowZombieWarningPrompt();
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && !interacted && this.enabled){
            inRange = true;
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
            prompt.HidePrompt();
        }        
    }
}
