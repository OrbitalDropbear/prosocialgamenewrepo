﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelManager_GasStation : MonoBehaviour
{
    GameManager gameManager;
    CustomCharacterController player;
    public SurveyControllerExample surveyScript;

    public bool madeDecision = false;
    public GameObject vendingMachineInteraction;
    bool vendingMachineActivated = false;
    public ParticleSystem dispenseCoins;
    public GameObject npcInteractionZone2;
    public GameObject GasCanInteractionZone;

    [Header("Objectives")]
    public TextMeshProUGUI objectivesText;
    string objectives1 = "o Get fuel from station";
    string objectives2 = "o Speak to the stranger";
    string objectives3 = "o Get water bottle";
    string objectives4 = "o Give water bottle";
    string objectives5 = "o Wait for the can to fill";
    string objectives6 = "o Grab the gas can";
    string objectives7 = "o Return to car with gas";

    public ScaleButtonPrompt zombiePrompt;
    public int zombieWarningTimer = 2;

    private void Awake() {
        gameManager = FindObjectOfType<GameManager>();
        player = FindObjectOfType<CustomCharacterController>();
        surveyScript = FindObjectOfType<SurveyControllerExample>();
        Camera.main.ResetCullingMatrix();
        objectivesText.text = "<color=white>" + objectives1;
        surveyScript.RecordDataTime("GasStationStart");
        
    }
    public void EndLevel(){
        StartCoroutine("EndOfGameSequence");
    }

    private IEnumerator EndOfGameSequence(){
        player.enabled = false;
        if(gameManager){
            yield return gameManager.fadeScreen();
            surveyScript.RecordData("PlayerHealthEndOfGasStation", player.GetComponent<PlayerHealth>().currentHealth.ToString());
            surveyScript.RecordStationEnd();
            if(SceneManager.GetActiveScene().buildIndex == 5){
                gameManager.NextLevel();
            } else {
                gameManager.NextLevel(Mathf.Abs(3-surveyScript.GetRandomCondition()) + 1);
            }
            
        }   
    }

    public void ShowZombieWarningPrompt(){
        StartCoroutine("ZombiesComingWarning");
    }

    private IEnumerator ZombiesComingWarning(){
        zombiePrompt.ShowPrompt();
        yield return new WaitForSeconds(zombieWarningTimer);
        zombiePrompt.HidePrompt(); 
    }

    public void ActivateVendingMachines(){
        if(!vendingMachineActivated){
            vendingMachineInteraction.SetActive(true);
            vendingMachineActivated = true;
            madeDecision = true;
            objectivesText.text = "<color=grey>" + objectives2 + "</color>\n<color=white>" + objectives3 + "</color>";
        }
    }

    public void ActivateSecondNPCInteraction(){
        if(gameManager){
            gameManager.SetCoinTotal(-40);
            gameManager.UpdateStartOfLevelCoinTotal();
        }
        dispenseCoins.Play();
        npcInteractionZone2.SetActive(true);
        
        objectivesText.text = "<color=grey>" + objectives2 + "</color>\n<color=grey>" + objectives3 + "</color>\n<color=white>" + objectives4 + "</color>";
    }

    public void ActivateGasCan(){
        GasCanInteractionZone.SetActive(true);
        gameManager.SetBigGunStatus();
        madeDecision = true;
        gameManager.SetDecisionMade();
        gameManager.SetStartingHealth(player.gameObject.GetComponent<PlayerHealth>().currentHealth);
        gameManager.SetMaxHealth(player.gameObject.GetComponent<PlayerHealth>().maxHealth);
        objectivesText.text = "<color=white>" + objectives1 + "</color>\n<color=grey>" + objectives2 + "</color>";

    }

    public void ActivateGasCanWithoutGun(){
        GasCanInteractionZone.SetActive(true);
        madeDecision = true;
        gameManager.SetDecisionMade();
        gameManager.SetStartingHealth(player.gameObject.GetComponent<PlayerHealth>().currentHealth);
        gameManager.SetMaxHealth(player.gameObject.GetComponent<PlayerHealth>().maxHealth);
        objectivesText.text = "<color=white>" + objectives1 + "</color>\n<color=grey>" + objectives2 + "</color>";

    }

    public void UpdateObjectivesListStranger(){
        objectivesText.text = "<color=white>" + objectives2 + "</color>";
    }

    public void UpdateObjectivesListHorde(){
        objectivesText.text = "<color=grey>" + objectives1 + "</color>\n<color=grey>" + objectives2 + "</color>\n<color=white>" + objectives5 + "</color>";
    }

    public void UpdateObjectivesListGasComplete(){
        objectivesText.text = "<color=white>" + objectives6 + "</color>";
    }

    public void UpdateObjectivesListCar(){
        objectivesText.text = "<color=grey>" + objectives6 + "</color>\n<color=white>" + objectives7 + "</color>";
    }
    
}
