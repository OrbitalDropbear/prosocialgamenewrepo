﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    
    LevelManager_GasStation levelManager;
    bool interacted = false;

    private void Awake() {
        levelManager = FindObjectOfType<LevelManager_GasStation>();
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Gas") && !interacted){
            interacted = true;
            other.GetComponent<PickupableObject>().DropObject();
            Destroy(other.gameObject);
            levelManager.EndLevel();
        }
    }
}
