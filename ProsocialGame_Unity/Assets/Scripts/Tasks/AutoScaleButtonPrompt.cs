﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoScaleButtonPrompt : ScaleButtonPrompt
{

    public int delay;
    public bool playOnAwake = true;
    public int scaleOverride;

    public bool lookAtCamera = false;
    public bool applyScaleFix = true;
    protected override void Awake() {
        finalScale = scaleOverride;

        base.Awake();

        if(applyScaleFix){
            scaleFix = finalScale;
        }
        
        if(playOnAwake)
        {
            Invoke("ShowPrompt", delay);
        }
        
    }

    protected override void Update() {
        if(lookAtCamera){
            transform.LookAt(Camera.main.transform);
        }
    }
}
