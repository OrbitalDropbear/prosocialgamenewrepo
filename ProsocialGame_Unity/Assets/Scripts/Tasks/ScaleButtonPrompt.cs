﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleButtonPrompt : MonoBehaviour
{
    
    Vector3 endScale;
    protected Vector3 startScale;
    Vector3 currentScale;
    Vector3 targetScale;
    private float moveDist;
    private float speed;
    protected int scaleFix = -2;
    protected int finalScale = 2;
    public float secondsToTarget;

    

    protected virtual void Awake() {
        endScale = Vector3.zero;
        startScale = Vector3.one * finalScale;
        scaleFix = -finalScale;
        startScale.x = scaleFix;
        GetComponent<RectTransform>().localScale = Vector3.zero;
    }

    protected virtual void Update() {
        transform.LookAt(Camera.main.transform);
    }
    public void ShowPrompt() {
        if(gameObject.activeInHierarchy){
            StopAllCoroutines();
            startScale = GetComponent<RectTransform>().localScale;
            endScale = Vector3.one * finalScale;
            endScale.x = scaleFix;
            StartCoroutine("Scale");
        }

    }

    public void HidePrompt(){
        if(gameObject.activeInHierarchy){
            StopAllCoroutines();
            startScale = GetComponent<RectTransform>().localScale;
            endScale = Vector3.zero;
            StartCoroutine("Scale");
        }
    }


    private IEnumerator Scale(){
        moveDist = 0;
        while(GetComponent<RectTransform>().localScale != endScale){
            speed = Vector3.Distance(startScale, endScale) / secondsToTarget;
            moveDist += speed * Time.deltaTime;
            float interpolationnValue = EasingFunctions.EaseOutQuad(Mathf.Clamp01(moveDist / Vector3.Distance(startScale, endScale)));
            GetComponent<RectTransform>().localScale = Vector3.LerpUnclamped(startScale, endScale, interpolationnValue);
            yield return null;
        }

    }

}
