﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car_Forest : MonoBehaviour
{
    LevelManager_Forest levelManager;

    ScaleButtonPrompt prompt;

    bool inRange;

    private void Awake() {
        levelManager = FindObjectOfType<LevelManager_Forest>();
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
    }


    private void Update() {
        
        if(inRange && Input.GetButtonDown("Interact")){
            levelManager.EndLevel();
        }
    }
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            inRange = true; 
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false; 
            prompt.HidePrompt();
        }
    }
}
