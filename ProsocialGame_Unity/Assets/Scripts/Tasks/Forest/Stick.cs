﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour
{
    public LevelManager_Forest levelManager;
    
    public ParticleSystem dust;

    bool inRange = false;
    ScaleButtonPrompt prompt;

    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();

    }
    private void Update() {
        if(Input.GetButtonDown("Interact") && inRange){
                levelManager.UpdateStickTask();
                Instantiate(dust, transform.position + dust.transform.position, dust.transform.rotation);
                Destroy(gameObject);
            }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player")){
            inRange = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")){
            inRange = true;
            prompt.ShowPrompt();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player")){
            inRange = false;
            prompt.HidePrompt();
        }
    }
}
