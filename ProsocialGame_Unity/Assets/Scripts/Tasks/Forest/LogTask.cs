﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogTask : MonoBehaviour
{
    public LevelManager_Forest levelManager;

    bool inRange = false;
    bool activatedAxe = false;

    private void Update() {
        if(Input.GetButtonDown("Interact") && inRange && !activatedAxe){
            levelManager.ActivateAxeTask();
            activatedAxe = true;
            inRange = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && gameObject.activeSelf){
            inRange = true;
        }
    }

    private void OnTriggerStay(Collider other) {
        if(other.CompareTag("Player") && gameObject.activeSelf){
            inRange = true;
        }
    }
    
    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
        }
    }

}
