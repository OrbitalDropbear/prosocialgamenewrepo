﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigSpot : MonoBehaviour
{
    
    public int coins = 30;
    
    public ParticleSystem coin_PS;

    GameObject shovel;

    GameManager gameManager;
    
    CustomCharacterController player;

    bool inRange = false;
    bool used = false;


    private void Awake()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<CustomCharacterController>();
    }

    private void Update() {

        if(Input.GetButtonDown("Interact") && inRange && !used){
                
            gameManager.SetCoinTotal(coins);
            
            Instantiate(coin_PS, transform.position + coin_PS.transform.position, coin_PS.transform.rotation);
            PlaceShovel();
            used = true;
        }  
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Shovel")){
            inRange = true;
            shovel = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Shovel")){
            shovel = null;
            inRange = false;
        }
    }

    void PlaceShovel(){
        if(shovel != null){
            shovel.GetComponent<PickupableObject>().DropObject();
            shovel.GetComponent<PickupableObject>().enabled = false;
        }
    }
}
