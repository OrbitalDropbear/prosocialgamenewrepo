﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AxeTask : MonoBehaviour
{
    public LevelManager_Forest levelManager;
    public GameObject axe;
    public ParticleSystem childParticle;
    public ParticleSystem dust;
    public GameObject axeCanvas;
    bool readyToCut = false;
    bool placed = false;
    CustomCharacterController player;
    ScaleButtonPrompt prompt;
    GameObject log;
    float timer;

    float yellowUpper = 1f;
    float yellowLower = .7f;
    float greenUpper = .9f;
    float greenLower = .8f;
    int hits = 0;
    bool stop = false;

    public Image swingSlider;

    bool inrange = false;
    bool logCut = false;
    
    private void Awake() {
        player = FindObjectOfType<CustomCharacterController>();
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
    }

    private void Update() {
        if(Input.GetButtonDown("Interact") && log != null && !stop && inrange){

            player.speed = 0;
            stop = true;
            prompt.HidePrompt();
            StartCoroutine("AxeSwing");

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(readyToCut && gameObject.activeSelf){
            if(other.CompareTag("Player")){
                inrange = true;
                prompt.ShowPrompt();
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Log") && gameObject.activeSelf){
            log = other.gameObject;
            if(!readyToCut){
                StartCoroutine("PlaceLog");
            } 
        }     
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inrange = false;
            prompt.HidePrompt();
        }
    }

    IEnumerator PlaceLog(){
        if(!placed){
            placed = true;
            log.GetComponent<PickupableObject>().DropObject();
            log.GetComponent<PickupableObject>().enabled = false;
            log.transform.parent = gameObject.transform;
            log.transform.position = transform.position + transform.up * .5f;
            log.transform.rotation = Quaternion.Euler(90, 0, 260);
            childParticle.gameObject.SetActive(true);
            yield return null;
            readyToCut = true;
        }        
    }

    IEnumerator AxeSwing(){
        axeCanvas.SetActive(true);
        childParticle.gameObject.SetActive(false);
        Vector3 originalPosition = axe.transform.position;
        Quaternion originalRotation = axe.transform.rotation;
        float end = -45;
        float start = 60;
        float angle = 0;
        float secondsToTarget = 2f;
        float moveDist = 0;
        float percentage = 0;
        logCut = false;
        
        axe.transform.position = transform.position + (-transform.right / 2) + (transform.up / 2);

        while (log != null && !Input.GetButton("Horizontal") && !Input.GetButton("Vertical") && !Input.GetButton("Fire1") && !Input.GetButton("Fire2")){
            swingSlider.fillAmount = 0;
            yield return null;
            while(!Input.GetButtonDown("Interact") && !Input.GetButton("Horizontal") && !Input.GetButton("Vertical") && !Input.GetButton("Fire1") && !Input.GetButton("Fire2")){
                float speed = (start - end) / secondsToTarget;
                moveDist += speed * Time.deltaTime;
                float interpolationnValue = EasingFunctions.EaseInQuad(Mathf.Clamp01(moveDist / (start - end)));
                angle = Mathf.LerpUnclamped(start, end, interpolationnValue);
                Quaternion temp = Quaternion.Euler(angle, 90, 0);
                axe.transform.rotation = temp;
                swingSlider.fillAmount = interpolationnValue;
                percentage = interpolationnValue;
                yield return null;
            }

            if((percentage >= yellowLower && percentage < greenLower) || (percentage > greenUpper && percentage <= yellowUpper)){
                hits++;
            } else if (percentage <= greenUpper && percentage >= greenLower){
                hits = 2;
            }

            moveDist = 0;
            float newstart = angle;
            float newend = start;

            while(angle != newend && !Input.GetButton("Horizontal") && !Input.GetButton("Vertical") && !Input.GetButton("Fire1") && !Input.GetButton("Fire2")){
                float speed = (newstart - newend) / ((secondsToTarget/5)*percentage);
                moveDist += speed * Time.deltaTime;
                float interpolationnValue = EasingFunctions.EaseInQuint(Mathf.Clamp01(moveDist / (newstart - newend)));
                angle = Mathf.LerpUnclamped(newstart, newend, interpolationnValue);
                Quaternion temp = Quaternion.Euler(angle, 90, 0);
                axe.transform.rotation = temp;
                yield return null;
            }

            if(hits >= 2){
                Instantiate(dust, log.transform.position, dust.transform.rotation);
                Destroy(log);
                hits = 0;
                logCut = true;
            }

            yield return new WaitForSeconds(.15f);

        }

        player.speed = 2.5f;
        axe.transform.position = originalPosition;
        axe.transform.rotation = originalRotation;
        axeCanvas.SetActive(false);
        childParticle.gameObject.SetActive(true);
        stop = false;
        if(logCut){
            placed = false;
            readyToCut = false;
            levelManager.UpdateLogTask(); //UpdateLogTask has to happen before MakeNextLogAvailable
            levelManager.MakeNextLogAvailable();
            gameObject.SetActive(false); 
        }   
    }

    
}


//&& !Input.GetButtonDown("Horizontal") && !Input.GetButtonDown("Vertical") && !Input.GetButtonDown("Fire1") && !Input.GetButtonDown("Fire2")

