﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleLookAt : MonoBehaviour
{

    public Transform lookAtTarget;
    public bool lookAtCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(lookAtCamera){
            transform.LookAt(Camera.main.transform);
        } else {
            transform.LookAt(lookAtTarget);
        }
        
    }
}
