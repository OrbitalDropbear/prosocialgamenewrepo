﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToSurveyScene : MonoBehaviour
{

    private void Start() {
        Cursor.lockState = CursorLockMode.None;
        GameManager.Instance.bGMusic.Stop();
        GameManager.Instance.questionnaireStarted = true;
    }
    
    public void NextLevel(){
        StartCoroutine("LoadNextLevel");
    }


    public IEnumerator LoadNextLevel()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }
}
