﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUIElement : MonoBehaviour
{
    [Header("Panel Moving")]
    public RectTransform onScreenTarget;
    public RectTransform offScreenTarget;
    public float secondsToTarget;
    public bool onScreen;
    private float moveDist;
    private float speed;
    private Vector3 startPos;
    private Vector3 endPos;


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<RectTransform>().position = offScreenTarget.position;
        onScreen = false;
    }

    public void Move(){

        if(onScreen){
            startPos = GetComponent<RectTransform>().position;
            endPos = offScreenTarget.position;
        } else {
            startPos = GetComponent<RectTransform>().position;
            endPos = onScreenTarget.position;
        }

        moveDist = 0;

        onScreen = !onScreen;

        StartCoroutine("Moving");
    }

    private IEnumerator Moving(){

        while(GetComponent<RectTransform>().position != endPos){
            speed = Vector3.Distance(startPos, endPos) / secondsToTarget;
            moveDist += speed * Time.deltaTime;
            float interpolationnValue = EasingFunctions.EaseOutElastic(Mathf.Clamp01(moveDist / Vector3.Distance(startPos, endPos)));
            GetComponent<RectTransform>().position = Vector3.LerpUnclamped(startPos, endPos, interpolationnValue);
            yield return null;
        }

    }
}
