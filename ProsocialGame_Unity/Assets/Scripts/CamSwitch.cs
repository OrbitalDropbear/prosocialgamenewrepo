﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CamSwitch : MonoBehaviour
{
    [SerializeField]
    private CinemachineVirtualCamera cam = default;
    
    // Start is called before the first frame update
    void Start()
    {
        cam.gameObject.SetActive(false);
    }

    private void OnTriggerStay(Collider other) {
        
        if(other.CompareTag("Player")){
            cam.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other) {
               
        if(other.CompareTag("Player")){
            cam.gameObject.SetActive(false);
        }
    }

}
