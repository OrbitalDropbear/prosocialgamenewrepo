﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class TalkingCamZone : MonoBehaviour
{

    [SerializeField]
    protected CinemachineVirtualCamera cam = default;

    public GameObject worldSpaceTalkingPrompt;
    public String dialogue;
    public GameObject npcName;

    public DialoguePanel dialoguePanel;
    protected bool inside = false;
    protected bool talking = false;
    private LevelManager_GasStation levelManager;
    protected int timesInteracted = 0;


    // Start is called before the first frame update
    void Start()
    {
        levelManager = FindObjectOfType<LevelManager_GasStation>();
        cam.gameObject.SetActive(false);
    }

    private void Update() {
        if(inside && !talking && Input.GetButtonDown("Interact")){
            cam.gameObject.SetActive(true);
            talking = true;
            dialoguePanel.Move();
            Cursor.lockState = CursorLockMode.None;
            timesInteracted++;
            StartCoroutine(PassDialogue(dialogue));
        }
    }

    private void OnTriggerEnter(Collider other) {
       
        if(other.CompareTag("Player")){
            inside = true;
        }
    }

    private void OnTriggerExit(Collider other) {
               
        if(other.CompareTag("Player")){
            cam.gameObject.SetActive(false);
            talking = false;
            inside = false;
            StopAllCoroutines();
            if(dialoguePanel.onScreen){
                dialoguePanel.Move();
                if(levelManager.madeDecision){
                    levelManager.surveyScript.RecordDataTime("SecondInteractionEnd x " + timesInteracted);
                    levelManager.surveyScript.RecordSecondTalkingEnd();
                }
            }
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    virtual protected IEnumerator PassDialogue (string dialogue){
        levelManager.surveyScript.RecordDataTime("SecondInteractionStart x " + timesInteracted);
        string[] sentences = dialogue.Split(';');
        int index = 0;
        yield return null;
        while(index < sentences.Length || !dialoguePanel.onScreen){
            if(sentences[index].Contains("`")){
                if(npcName) npcName.SetActive(true);
                sentences[index] = sentences[index].TrimStart('`');
            }
            dialoguePanel.DisplayDialogue(sentences[index]);
            if(Input.GetButtonDown("Interact")){                
                index++;
            }
            yield return null;
        }
        dialoguePanel.Move();
        talking = false;
        cam.gameObject.SetActive(false);
        if(npcName) npcName.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        levelManager.surveyScript.RecordDataTime("SecondInteractionEnd x " + timesInteracted);
        levelManager.surveyScript.RecordSecondTalkingEnd();
    }
}
