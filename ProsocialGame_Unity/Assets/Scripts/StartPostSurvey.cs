﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPostSurvey : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
      GameObject.FindObjectOfType<SurveyControllerExample>().ShowPostTestSurvey(); 
    }

}
