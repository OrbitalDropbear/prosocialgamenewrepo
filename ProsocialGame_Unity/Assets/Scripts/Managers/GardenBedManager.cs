﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenBedManager : MonoBehaviour
{
    
    public List<GameObject> weeds;
    public LevelManager_Forest levelManager;
    public ParticleSystem dust;
    //public int levelManagerParticleListIndexLocation;
    bool inRange = false;
    ScaleButtonPrompt prompt;
    
    private void Awake() {
        prompt = GetComponentInChildren<ScaleButtonPrompt>();        
    }

    private void Update() {
        if(inRange && Input.GetButtonDown("Interact")){
            foreach (GameObject weed in weeds)
            {
                Instantiate(dust, weed.transform.position, dust.transform.rotation);
                Destroy(weed);
            }
            Destroy(gameObject);
            levelManager.UpdateGardenTask(/*levelManagerParticleListIndexLocation*/);

        }
    }


    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player")){
            
            inRange = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")){
            
            inRange = true;
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player")){
            
            inRange = false;
            prompt.HidePrompt();
        }
    }
}
