﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListManager : MonoBehaviour
{
    LevelManager_Forest levelManager;
    GameManager gameManager;

    bool inRange = false;
    public GameObject toDoList;
    public ParticleSystem coin_PS;

    ScaleButtonPrompt prompt;

    private void Awake() {
        gameManager = FindObjectOfType<GameManager>();
        levelManager = FindObjectOfType<LevelManager_Forest>();
        prompt = GetComponentInChildren<ScaleButtonPrompt>();
    }

    private void Update() {

        if(inRange && Input.GetButtonDown("Interact")){
            if(!levelManager.GetTasksActive()){
                levelManager.TurnOnTasks();
            }

            if(levelManager.CoinsAvailable()){
                gameManager.SetCoinTotal(levelManager.DispenseCoins());
                Instantiate(coin_PS, transform.position + coin_PS.transform.position, coin_PS.transform.rotation);
                levelManager.CheckTaskCompletion();
            } else {
                toDoList.GetComponent<MoveUIElement>().Move();
            }
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")){
            inRange = true;
            prompt.ShowPrompt();
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player")){
            inRange = false;
            if(toDoList.GetComponent<MoveUIElement>().onScreen){
                toDoList.GetComponent<MoveUIElement>().Move();
            }
            prompt.HidePrompt();
        }        
    }
}
