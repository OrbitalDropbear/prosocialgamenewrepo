using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{

    private static GameManager _instance;

    public static GameManager Instance { get { return _instance; } }
    
    Image fadeImage;
    Image muteImage;
    TextMeshProUGUI coinText;
    TextMeshProUGUI coinChangeText;
    public float fadeSpeed = 1;
    public float countDuration;
    public AudioSource bGMusic;

    bool muted = false;
    [HideInInspector]
    public bool questionnaireStarted = false;

    bool startWithBigGun = false;
    bool madeDecision = false;
    bool vendingMachineDestroyed = false;
    int startingHealth = 100;
    int maxHealth = 100;

    int randomisedCondition = 0;

    float coinTotal = 0;
    float coinUITotal = 0;
    float exitTime = 2;
    float startOfLevelCoinTotal = 0;

    //Game Analytics Variables
    int totalKills = 0;
    int bulletsFired = 0;
    int deaths = 0;

    private void Awake() {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject); 
    }
    
    private void OnEnable() {
        fadeImage = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
        muteImage = GameObject.FindGameObjectWithTag("Quit").GetComponent<Image>();
        coinText = GameObject.FindGameObjectWithTag("CoinTotal").GetComponent<TextMeshProUGUI>();
        coinChangeText = GameObject.FindGameObjectWithTag("CoinChange").GetComponent<TextMeshProUGUI>();
        muteImage.gameObject.SetActive(false); 
        coinChangeText.gameObject.SetActive(false);
        randomisedCondition = Random.Range(0, 2); //0 = No reward, 1 = Reward Surprise, 2 = Reward Promise
    }

//#if UNITY_STANDALONE
    private void Update() {
        
        if(Input.GetKeyDown(KeyCode.Escape) && !muted && !questionnaireStarted){
            //StartCoroutine(MuteGame());
            muteImage.gameObject.SetActive(true);
            muteImage.fillAmount = 1;
            bGMusic.Pause();
            muted = true;
        } else if (Input.GetKeyDown(KeyCode.Escape) && muted && !questionnaireStarted){
            muteImage.gameObject.SetActive(false);
            muteImage.fillAmount = 0;
            bGMusic.Play();
            muted = false;
        }
            
    }

    IEnumerator MuteGame(){
        float timer = 0;
        muteImage.gameObject.SetActive(true);
        muteImage.fillAmount = 0;
        while(Input.GetKey(KeyCode.Escape)){
            timer += Time.deltaTime;
            muteImage.fillAmount = timer / exitTime;
            if(timer >= exitTime){
                bGMusic.Pause();
                muted = true;
            }
            yield return null;
        }
        timer = 0;
        if(!muted){
            muteImage.gameObject.SetActive(false);
            muteImage.fillAmount = 0;
        }

        yield return null;
    }
//#endif

    public void SetCoinTotal(int amount, bool addToExisting = true){

        if(addToExisting){
            coinTotal += amount;
        } else {
            coinTotal = amount;
        }

        StartCoroutine(ChangeCoinUI());
    }

    public void UpdateStartOfLevelCoinTotal(){
        startOfLevelCoinTotal = coinTotal;
    }

    IEnumerator ChangeCoinUI(){
        float timer = 0;
        float changeAmount = coinTotal - coinUITotal;
        float currentUIAmount = coinUITotal;

        coinChangeText.gameObject.SetActive(true);
        coinChangeText.GetComponent<RectTransform>().localScale = Vector3.one;
        string sign = Mathf.Sign(changeAmount) == 1 ? "+" : "";
        coinChangeText.text = $"{sign}{(int)changeAmount}";
        Vector3 coinChangeScale = Vector3.one;
        yield return new WaitForSeconds (.3f);
        
        while(coinUITotal != coinTotal){
            timer += Time.deltaTime;
            
            float interpolationnValue = EasingFunctions.EaseOutQuad(Mathf.Clamp01(timer / countDuration));
            float scaleInterpolationnValue = EasingFunctions.EaseInExponential(Mathf.Clamp01(timer / countDuration));
            
            coinUITotal = Mathf.LerpUnclamped(currentUIAmount, coinTotal, interpolationnValue);
            coinChangeScale = Vector3.LerpUnclamped(Vector3.one, Vector3.zero, scaleInterpolationnValue);
            float coinChangeAmount = Mathf.LerpUnclamped(changeAmount, 0, interpolationnValue);

            coinChangeText.GetComponent<RectTransform>().localScale = coinChangeScale;
            sign = Mathf.Sign(coinChangeAmount) == 1 ? "+" : "";
            coinChangeText.text = $"{sign}{(int)coinChangeAmount}";
            coinText.text = $"{(int)coinUITotal}";

            yield return null;    
        }
    }

    public void NextLevel(){
        StartCoroutine(LoadNextLevel(1));
    }

    public void NextLevel(int buildIndexModifer){
        StartCoroutine(LoadNextLevel(buildIndexModifer));
    }
    public IEnumerator LoadNextLevel(int buildIndexModifier = 1)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + buildIndexModifier);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        yield return new WaitForSeconds(.25f);
        fadeImage = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
        muteImage = GameObject.FindGameObjectWithTag("Quit").GetComponent<Image>();
        coinText = GameObject.FindGameObjectWithTag("CoinTotal").GetComponent<TextMeshProUGUI>();
        coinChangeText = GameObject.FindGameObjectWithTag("CoinChange").GetComponent<TextMeshProUGUI>();
        startOfLevelCoinTotal = coinTotal;
        coinText.text = $"{(int)coinTotal}";
        coinChangeText.gameObject.SetActive(false);
        if(muteImage)
            muteImage.gameObject.SetActive(false);
        
        if(fadeImage){
            StartCoroutine(fadeScreen(false));
        } else {
            GetComponentInChildren<AudioSource>().Stop();
        }

    }

    public IEnumerator fadeScreen(bool fadeToBlack = true){
        if (fadeToBlack){
            fadeImage.color = Color.black;
            while(fadeImage.fillAmount < 1){
                fadeImage.fillAmount += Time.deltaTime * fadeSpeed;
                yield return null;
            }
        } else {
            while(fadeImage.fillAmount > 0){
                fadeImage.fillAmount -= Time.deltaTime * fadeSpeed;
                yield return null;
            }
        }
    }

    public void SetBigGunStatus(){
        startWithBigGun = true;
        FindObjectOfType<PlayerShooting>().GiveBigGun();
        FindObjectOfType<CustomCharacterController>().SwapGuns();
    }

    public bool GetBigGunStatus(){
        return startWithBigGun;
    }

    public void ReloadLevelMethod(){
        StartCoroutine("ReloadLevel");
    }

    IEnumerator ReloadLevel()
    {
        AsyncOperation asyncLoad;
        if(madeDecision){
            asyncLoad = SceneManager.LoadSceneAsync(5); //load the GasStation - PostDecisionSave scene
        } else {
            asyncLoad = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        yield return new WaitForSeconds(.25f);
        
        fadeImage = GameObject.FindGameObjectWithTag("Fade").GetComponent<Image>();
        muteImage = GameObject.FindGameObjectWithTag("Quit").GetComponent<Image>();
        muteImage.gameObject.SetActive(false);
        coinText = GameObject.FindGameObjectWithTag("CoinTotal").GetComponent<TextMeshProUGUI>();
        coinChangeText = GameObject.FindGameObjectWithTag("CoinChange").GetComponent<TextMeshProUGUI>();
        coinText.text = $"{(int)startOfLevelCoinTotal}";
        SetCoinTotal((int)startOfLevelCoinTotal, false);
        coinChangeText.gameObject.SetActive(false);
        
        if(fadeImage){
            StartCoroutine(fadeScreen(false));
        } else {
            GetComponentInChildren<AudioSource>().Stop();
        }
        //yield return new WaitForSeconds(5f);

        /*GameManager[] gameManagers = FindObjectsOfType<GameManager>();
        foreach(GameManager gm in gameManagers){
            if(gm != this){
                Destroy(gm);
            }
        }*/
    }

    public int GetMaxHealth(){
        return maxHealth;
    }

    public void SetMaxHealth(int amount){
        maxHealth = amount;
    }

    public int GetStartingHealth(){
        return startingHealth;
    }

    public void SetStartingHealth(int amount){
        startingHealth = amount;
    }

    public void SetDecisionMade(){
        madeDecision = true;
    }

    public bool GetDecisionMade(){
        return madeDecision;
    }

    public void SetVendingMachine(){
        vendingMachineDestroyed = true;
    }

    public bool GetVendingMachine (){
        return vendingMachineDestroyed;
    }

    public void IncrementTotalKills(){
        totalKills++;
    }

    public void IncrementBulletsFired(){
        bulletsFired++;
    }

    public void IncrementDeaths(){
        deaths++;
    }


    public int GetTotalKills(){
        return totalKills;
    }

    public int GetBulletsFired(){
        return bulletsFired;
    }

    public int GetDeaths(){
        return deaths;
    }



    
}
