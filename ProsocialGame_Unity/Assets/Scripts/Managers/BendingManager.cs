﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

//[ExecuteAlways]
public class BendingManager : MonoBehaviour
{
    #region Constants

    private const string BENDING_FEATURE = "ENABLE_BENDING";

    #endregion


    #region MonoBehaviour

    private void Awake() {
        if(Application.isPlaying){
            Shader.EnableKeyword(BENDING_FEATURE);
        } else {
            Shader.DisableKeyword(BENDING_FEATURE);
        }
    }

    private void OnEnable() {
        //Attach to the camera rendering functions
        RenderPipelineManager.beginCameraRendering += OnBeginCameraRendering;
        RenderPipelineManager.endCameraRendering += OnEndCameraRendering;
    }

    private void OnDisable() {
        //Detach from the camera rendering functions
        RenderPipelineManager.beginCameraRendering -= OnBeginCameraRendering;
        RenderPipelineManager.endCameraRendering -= OnEndCameraRendering;       
    }

    #endregion

    #region Methods

    private static void OnBeginCameraRendering(ScriptableRenderContext context, Camera camera){
        //Generate new culling frustum distinct from the view frustum
        //camera.cullingMatrix = Matrix4x4.Ortho(-99, 99, -99, 99, 0.001f, 99) * camera.worldToCameraMatrix;
        camera.ResetCullingMatrix();
    }

    private static void OnEndCameraRendering(ScriptableRenderContext context, Camera cam){
        //Set the culling frustum back to normal
        cam.ResetCullingMatrix();

    }

    #endregion
}
