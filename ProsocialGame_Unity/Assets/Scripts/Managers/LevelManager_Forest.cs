﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelManager_Forest : MonoBehaviour
{

    CustomCharacterController player;
    GameManager gameManager;

    [Header("Tasks")]
    public List<GameObject> stickParticles;
    public List<GameObject> gardenParticles;
    public List<GameObject> logs;
    public GameObject logParticles;
    public AxeTask axeTask;
    public List<Collider> taskColliders;
    public Collider carCollider;
    public GameObject carParticles;
    public GameObject listParticle;
    public TextMeshProUGUI logCounterUI;
    public TextMeshProUGUI stickCounterUI;
    public TextMeshProUGUI gardenCounterUI;
    

    [Header("Dialogue")]
    public string startingText;
    public string endingText;
    public GameObject talkingCamera;
    DialoguePanel dialoguePanel;

    [Header("Controls Stuff")]
    public GameObject controlPanel;
    public GameObject worldControls;

    [Header("Objectives")]
    public TextMeshProUGUI objectivesText;
    string objectives1 = "o Get a feel for the controls";
    string objectives2 = "o Complete all tasks";
    string objectives3 = "o Use the car to get fuel";

    int logsPickedup = 0;
    int gardenTasksCompleted = 0;
    int sticksCollected = 0;
    int logsChopped = 0;
    int coinsToDispense = 0;

    float playerBaseSpeed = 2.5f;
    bool tasksActive = false;
    bool logsComplete = false;
    bool sticksComplete = false;
    bool gardensComplete = false;

    private SurveyControllerExample surveyScript;

    private void Awake()
    {
        surveyScript = FindObjectOfType<SurveyControllerExample>();
        if(surveyScript){
            surveyScript.RecordDataTime("ForestStart");
        }
        foreach (GameObject stickParticle in stickParticles)
        {
            stickParticle.SetActive(false);
        }
        foreach (GameObject gardenParticle in gardenParticles)
        {
            gardenParticle.SetActive(false);
        }
        foreach (Collider collider in taskColliders)
        {
            collider.enabled = false;
        }
        MakeNextLogAvailable();

        player = FindObjectOfType<CustomCharacterController>();
        logParticles.SetActive(false);
        dialoguePanel = FindObjectOfType<DialoguePanel>();
        gameManager = FindObjectOfType<GameManager>();

    }

    private void Start() {
        StartCoroutine("StartOfGameSequence");
    }

    public void TurnOnTasks(){
        tasksActive = true;
        //worldControls.SetActive(false);
        objectivesText.text = "<color=grey>" + objectives1 + "</color>\n<color=white>" + objectives2 + "</color>";
        listParticle.SetActive(false);
        foreach (GameObject stickParticle in stickParticles)
        {
            stickParticle.SetActive(true);
        }
        foreach (GameObject gardenParticle in gardenParticles)
        {
            gardenParticle.SetActive(true);
        }
        foreach (Collider collider in taskColliders)
        {
            collider.enabled = true;
        }
        logParticles.SetActive(true);
        MakeNextLogAvailable();
    }

    public void UpdateGardenTask(/*int listIndexLocation*/){
        gardenTasksCompleted++;
        gardenCounterUI.text = string.Format("{0}/4", gardenTasksCompleted);
        if(gardenTasksCompleted == 4){
            coinsToDispense += 30;
            gardensComplete = true;
            IsListNextObjective();
            surveyScript.RecordDataTime("Garden Task Finished");
        }
    }

    public void MakeCarAvailble(){
        carCollider.enabled = true;
        carParticles.SetActive(true);

    }

    public void UpdateStickTask(){
        sticksCollected++;
        stickCounterUI.text = string.Format("{0}/7", sticksCollected);
        if(sticksCollected == 7){
            coinsToDispense += 30;
            sticksComplete = true;
            IsListNextObjective();
            surveyScript.RecordDataTime("Stick Task Finished");
        }
    }

    public void UpdateLogTask(){
        logsChopped++;
        logCounterUI.text = string.Format("{0}/3", logsChopped);
        if(logsChopped == 3){
            coinsToDispense += 30;
            logsComplete = true;
            IsListNextObjective();
            surveyScript.RecordDataTime("Log Task Finished");
        }
    }

    public void IsListNextObjective(){
        if(logsComplete && sticksComplete && gardensComplete){
            listParticle.SetActive(true);
        }
    }

    public void CheckTaskCompletion(){
        if(logsComplete && sticksComplete && gardensComplete){
            MakeCarAvailble();
            objectivesText.text = "<color=grey>" + objectives2 + "</color>\n<color=white>" + objectives3 + "</color>";
            listParticle.SetActive(false);
            IEnumerator newCoRu = PassDialogue(endingText);
            StartCoroutine(newCoRu);
        } 
    }

    public void EndLevel(){
        if(logsComplete && sticksComplete && gardensComplete){
            carParticles.SetActive(false);
            StartCoroutine("EndOfGameSequence");
        }
    }

    public bool GetTasksActive(){
        return tasksActive;
    }

    public bool CoinsAvailable(){
        return coinsToDispense != 0;
    }

    public int DispenseCoins(){
        int temp = coinsToDispense;
        coinsToDispense = 0;
        return temp;
    }

    private IEnumerator StartOfGameSequence(){
        player.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        controlPanel.SetActive(false);
        yield return gameManager.fadeScreen(false);
        talkingCamera.SetActive(true);
        dialoguePanel.Move();
        IEnumerator newCoRu = GameDialogue(startingText);
        StartCoroutine(newCoRu);
    }

    private IEnumerator EndOfGameSequence(){
        player.speed = 0;
        talkingCamera.SetActive(true);
        //dialoguePanel.Move();
        //IEnumerator newCoRu = GameDialogue(endingText);
        //StartCoroutine(newCoRu);
        yield return gameManager.fadeScreen();
        surveyScript.RecordData("PlayerHealthEndOfForest", player.GetComponent<PlayerHealth>().currentHealth.ToString());
        surveyScript.RecordData("#ForestKills", gameManager.GetTotalKills().ToString());
        surveyScript.RecordData("#ForestShots", gameManager.GetBulletsFired().ToString());
        surveyScript.RecordStationStart();
        gameManager.NextLevel(1 + surveyScript.GetRandomCondition());
    }

    private IEnumerator GameDialogue (string dialogue){
        string[] sentences = dialogue.Split(';');
        int index = 0;
        yield return null;
        while(index < sentences.Length || !dialoguePanel.onScreen){
            dialoguePanel.DisplayDialogue(sentences[index]);
            if(Input.GetButtonDown("Interact")){                
                index++;
            }
            yield return null;
        }

        if(tasksActive){ //end of level
            //dialoguePanel.Move();
            Debug.Log("GameDialogue coroutine branch that should never be reached");
        } else { //start of level
            dialoguePanel.Move();
            player.enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
            controlPanel.SetActive(true);
            talkingCamera.SetActive(false);
        }
        
    }


    private IEnumerator PassDialogue (string dialogue){
        string[] sentences = dialogue.Split(';');
        int index = 0;
        yield return null;
        dialoguePanel.Move();
        while(index < sentences.Length || !dialoguePanel.onScreen){
            dialoguePanel.DisplayDialogue(sentences[index]);
            if(Input.GetButtonDown("Interact")){                
                index++;
            }
            yield return null;
        }
        dialoguePanel.Move();
    }

    /// <summary>
    /// Log task stuff
    /// </summary>
    public void MakeNextLogAvailable(){
        if(logsChopped < logs.Count){
            logParticles.gameObject.SetActive(true);
            logs[logsChopped].GetComponent<PickupableObject>().enabled = true;
            logs[logsChopped].GetComponent<LogTask>().enabled = true;
        } else {
            logParticles.gameObject.SetActive(false);
        }
    }

    public void ActivateAxeTask(){
        axeTask.gameObject.SetActive(true);
        axeTask.GetComponentInChildren<Transform>().gameObject.SetActive(true);
        logParticles.gameObject.SetActive(false);
    }

    
}
