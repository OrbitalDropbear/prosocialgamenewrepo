﻿using UnityEngine;

public static class EasingFunctions {

	#region HelperCode

	private const float PI = Mathf.PI;
    private const float HALF_PI = Mathf.PI / 2;

	// Easing function signature for the array
	private delegate float EasingFunction(float x);
	// Array storing all the easing functions
	private static readonly EasingFunction[] easingFunctions = new EasingFunction[] {
		Linear,
		EaseInQuad,
		EaseOutQuad,
		EaseInOutQuad,
		EaseInCubic,
		EaseOutCubic,
		EaseInOutCubic,
		EaseInQuart,
		EaseOutQuart,
		EaseInOutQuart,
		EaseInQuint,
		EaseOutQuint,
		EaseInOutQuint,
		EaseInSin,
		EaseOutSin,
		EaseInOutSin,
		EaseInCircular,
		EaseOutCircular,
		EaseInOutCircular,
		EaseInExponential,
		EaseOutExponential,
		EaseInOutExponential,
		EaseInElastic,
		EaseOutElastic,
		EaseInOutElastic,
		EaseInBack,
		EaseOutBack,
		EaseInOutBack,
		EaseInBounce,
		EaseOutBounce,
		EaseInOutBounce																	
	};
	
	// Enum storing all easing functions, for easing testing and debugging
    public enum Function {
        Linear,
        EaseInQuad,
        EaseOutQuad,
        EaseInOutQuad,
        EaseInCubic,
        EaseOutCubic,
        EaseInOutCubic,
        EaseInQuart,
        EaseOutQuart,
        EaseInOutQuart,
        EaseInQuint,
        EaseOutQuint,
        EaseInOutQuint,
        EaseInSin,
        EaseOutSin,
        EaseInOutSin,
        EaseInCircular,
        EaseOutCircular,
        EaseInOutCircular,
        EaseInExponential,
        EaseOutExponential,
        EaseInOutExponential,
        EaseInElastic,
        EaseOutElastic,
        EaseInOutElastic,
        EaseInBack,
        EaseOutBack,
        EaseInOutBack,
        EaseInBounce,
        EaseOutBounce,
        EaseInOutBounce,
        Length
    }

	// Stores the readable names for ease of displaying on screen
	private static readonly string[] functionNames = new string[] {
		"Linear",
		"Ease In Quad",
		"Ease Out Quad",
		"Ease In Out Quad",
		"Ease In Cubic",
		"Ease Out Cubic",
		"Ease In Out Cubic",
		"Ease In Quart",
		"Ease Out Quart",
		"Ease In Out Quart",
		"Ease In Quint",
		"Ease Out Quint",
		"Ease In Out Quint",
		"Ease In Sin",
		"Ease Out Sin",
		"Ease In Out Sin",
		"Ease In Circular",
		"Ease Out Circular",
		"Ease In Out Circular",
		"Ease In Exponential",
		"Ease Out Exponential",
		"Ease In Out Exponential",
		"Ease In Elastic",
		"Ease Out Elastic",
		"Ease In Out Elastic",
		"Ease In Back",
		"Ease Out Back",
		"Ease In Out Back",
		"Ease In Bounce",
		"Ease Out Bounce",
		"Ease In Out Bounce"
	};

	// Calls an easing function from enum
    public static float Ease(float x, Function f) {
		int index = (int)f;
		if (index < 0 || index >= easingFunctions.Length) throw new System.Exception("Easing function '" + f.ToString() + "' does not exist");

		return easingFunctions[index](x);
    }

	// Gets the name of an easing function from enum
	public static string GetName(Function f) {
		int index = (int)f;
		if (index < 0 || index >= easingFunctions.Length) throw new System.Exception("Easing function '" + f.ToString() + "' does not exist");

		return functionNames[index];
	}

	#endregion


	// --------- Easing function definitions --------- \\

	public static float Linear(float x) {
		return x;
	}

    public static float EaseInQuad(float x) {
        return x * x;
    }
    public static float EaseOutQuad(float x) {
        return x * (2 - x);
    }
    public static float EaseInOutQuad(float x) {
        return x < 0.5f ? 2 * x * x : -1 + (4 - 2 * x)*x;
    }

    public static float EaseInCubic(float x) {
        return x * x * x;
    }
    public static float EaseOutCubic(float x) {
        return (--x) * x * x + 1;
    }
    public static float EaseInOutCubic(float x) {
        return x < 0.5f ? 4 * x * x * x : (x - 1) * (2 * x - 2) * (2 * x - 2) + 1;
    }

    public static float EaseInQuart(float x) {
        return x * x * x * x;
    }
    public static float EaseOutQuart(float x) {
        return 1 - (--x) * x * x * x;
    }
    public static float EaseInOutQuart(float x) {
        return x < 0.5f ? 8 * x * x * x * x : 1 - 8 * (--x) * x * x * x;
    }

    public static float EaseInQuint(float x) {
        return x * x * x * x * x;
    }
    public static float EaseOutQuint(float x) {
        return 1 + (--x) * x * x * x * x;
    }
    public static float EaseInOutQuint(float x) {
        return x < 0.5f ? 16 * x * x * x * x * x : 1 + 16 * (--x) * x * x * x * x;
    }

    public static float EaseInSin(float x) {
        return Mathf.Sin((x - 1) * HALF_PI) + 1;
    }
    public static float EaseOutSin(float x) {
        return Mathf.Sin(x * HALF_PI);
    }
    public static float EaseInOutSin(float x) {
        return 0.5f * (1 - Mathf.Cos(x * PI));
    }

    public static float EaseInCircular(float x) {
        return 1 - Mathf.Sqrt(1 - (x * x));
    }
    public static float EaseOutCircular(float x) {
        return Mathf.Sqrt((2 - x) * x);
    }
    public static float EaseInOutCircular(float x) {
        return x < 0.5f ? 0.5f * (1 - Mathf.Sqrt(1 - 4 * (x * x))) : 0.5f * (Mathf.Sqrt(-((2 * x) - 3) * ((2 * x) - 1)) + 1);
    }

    public static float EaseInExponential(float x) {
        return (x == 0) ? x : Mathf.Pow(2, 10 * (x - 1));
    }
    public static float EaseOutExponential(float x) {
        return (x == 1) ? x : 1 - Mathf.Pow(2, -10 * x);
    }
    public static float EaseInOutExponential(float x) {
        if (x == 0 || x == 1) return x;
        return x < 0.5f ? 0.5f * Mathf.Pow(2, (20 * x) - 10) : -0.5f * Mathf.Pow(2, (-20 * x) + 10) + 1;
    }

    public static float EaseInElastic(float x) {
        return Mathf.Sin(13 * HALF_PI * x) * Mathf.Pow(2, 10 * (x - 1));
    }
    public static float EaseOutElastic(float x) {
        return Mathf.Sin(-13 * HALF_PI * (x + 1)) * Mathf.Pow(2, -10 * x) + 1;
    }
    public static float EaseInOutElastic(float x) {
        return x < 0.5f ? 0.5f * Mathf.Sin(13 * HALF_PI * (2 * x)) * Mathf.Pow(2, 10 * ((2 * x)-1)) :
            0.5f * (Mathf.Sin(-13 * HALF_PI * ((2 * x -1) + 1)) * Mathf.Pow(2, -10 * (2 * x - 1)) + 2);
    }

    public static float EaseInBack(float x) {
        return x * x * x - x * Mathf.Sin(x * PI);
    }

    public static float EaseOutBack(float x) {
        x = 1 - x;
        return 1- (x * x * x - x * Mathf.Sin(x * PI));
    }
    public static float EaseInOutBack(float x) {
        float y = x < 0.5f ? 2 * x : 1 - (2 * x - 1);
        return x < 0.5f ? 0.5f * (y * y * y - y * Mathf.Sin(y * PI)) : 0.5f * (1 - (y * y * y - y * Mathf.Sin(y * PI))) + 0.5f;
    }

    public static float EaseInBounce(float x) {
        return 1 - EaseOutBounce(1 - x);
    }
    public static float EaseOutBounce(float x) {
        if (x < 4 / 11.0f) return (121 * x * x) / 16.0f;
        else if (x < 8 / 11.0f) return (363 / 40.0f * x * x) - (99 / 10.0f * x) + 17 / 5.0f;
        else if (x < 9 / 10.0f) return (4356 / 361.0f * x * x) - (35442 / 1805.0f * x) + 16061 / 1805.0f;
        else return (54 / 5.0f * x * x) - (513 / 25.0f * x) + 268 / 25.0f;
    }
    public static float EaseInOutBounce(float x) {
        return x < 0.5f ? 0.5f * EaseInBounce(x * 2) : 0.5f * EaseOutBounce(x * 2 - 1) + 0.5f;
    }
}
