﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnZone : MonoBehaviour
{

    public GameObject horde;

    bool activated;

    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player") && !activated){
            horde.SetActive(true);
            activated = true;
        }
            
    }

}
