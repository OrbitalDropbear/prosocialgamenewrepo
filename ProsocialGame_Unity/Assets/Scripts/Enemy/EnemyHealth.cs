﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public AudioClip deathClip;

    int currentHealth;
    Animator anim;
    AudioSource enemyAudio;
    public List<AudioClip> audioClips;
    ParticleSystem hitParticles;
    CapsuleCollider capsuleCollider;
    EnemyMovement enemyMovement;
    bool sinking = false;
    GameManager gameManager;

    void Awake ()
    {
        anim = GetComponent <Animator> ();
        enemyAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
        capsuleCollider = GetComponent <CapsuleCollider> ();
        enemyMovement = this.GetComponent<EnemyMovement>();
        gameManager = FindObjectOfType<GameManager>();
    }

    void OnEnable()
    {
        currentHealth = startingHealth;
        //SetKinematics(false);
    }

    // private void SetKinematics(bool isKinematic)
    // {
    //     capsuleCollider.isTrigger = isKinematic;
    //     capsuleCollider.attachedRigidbody.isKinematic = isKinematic;
    // }

    void Update ()
    {
        if (IsDead())
        { 
            if(sinking){
                transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
            }
            if (transform.position.y < -10f)
            {
                Destroy(this.gameObject);
            }
        }
    }

    public bool IsDead()
    {
        return (currentHealth <= 0f);
    }

    public void TakeDamage (int amount, Vector3 hitPoint)
    {
        if (!IsDead())
        {
            currentHealth -= amount;

            if (IsDead()) {
                StartCoroutine("HandleDeath");
            } else {
                enemyMovement.GoToPlayer();
            }
        }

        enemyAudio.PlayOneShot(audioClips[Random.Range(0, 3)]);    
        hitParticles.transform.position = hitPoint;
        hitParticles.Play();
    }

    public void StartSinking ()
    {
        
        sinking = true;
        GetComponent <UnityEngine.AI.NavMeshAgent> ().enabled = false;
        //SetKinematics(true);

        //ScoreManager.score += scoreValue;
    }

    public int CurrentHealth()
    {
        return currentHealth;
    }

    IEnumerator HandleDeath(){
        //enemyAudio.clip = deathClip;
        //enemyAudio.Play ();
        anim.SetTrigger ("Dead");
        gameManager.IncrementTotalKills();
        enemyAudio.clip = deathClip;
        enemyAudio.Play();
        if(gameObject.name.Contains("Walk")){
            CapsuleCollider capsule = GetComponent<CapsuleCollider>();
            capsule.height = 1;
            capsule.direction = 2;
            capsule.center = new Vector3(-0.5f, 0, 1);
        }
        yield return new WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length + anim.GetCurrentAnimatorStateInfo(0).normalizedTime);
        StartSinking();
    }
}