﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerForest : MonoBehaviour
{
    [Tooltip("Determines the chance that a walker or a crawler spawns. 0 for always walker. 0.5 for 50/50 chance. 1 for always crawler")]
    [Range(0f, 1f)]
    public float walkerOrCrawler;

    public GameObject walker;
    public GameObject crawler;

    float timer;
    public float spawnRate = 10;
    bool enemyNearby = false;
    LevelManager_Forest levelManager;

    private void Awake() {
        timer = Random.Range(-5, 3);
        levelManager = FindObjectOfType<LevelManager_Forest>();
    }
    private void Update() {
        if(timer >= spawnRate && !enemyNearby && levelManager.GetTasksActive()){
            timer = 0;
            enemyNearby = true;
            if(Random.value >= walkerOrCrawler){
                GameObject newEnemy = Instantiate(walker, transform.position, walker.transform.rotation) as GameObject;
                newEnemy.GetComponent<EnemyMovement>().SetEnemySpeed(Random.Range(.25f, .75f));
            } else {
                Instantiate(crawler, transform.position, crawler.transform.rotation);
            }
        }
        timer += Time.deltaTime;
    }
}
