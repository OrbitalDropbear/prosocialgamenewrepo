﻿using System;
using System.Collections;
using System.Collections.Generic;
using ECM.Controllers;
using ECM.Examples.Common;
using UnityEngine;

public sealed class CustomCharacterController : BaseCharacterController
{

//Asset Variables
    [Header("CUSTOM CONTROLLER")]
    [Tooltip("The character's follow camera.")]
    public Transform playerCamera;

    [Tooltip("The character's walk speed.")]
    [SerializeField]
    private float _walkSpeed = 2.5f;

    [Tooltip("The character's run speed.")]
    [SerializeField]
    private float _runSpeed = 5.0f;

    private float _targetYaw;
    private float _yaw;
    private float _yawVelocity;

    public float time2TurnTarget = 0.1f;


//Blake's Variables
    Animator anim;

    // Ray lookRay = new Ray();
    // RaycastHit lookHit;
    // int range = 100;
    int lookMask;

    [Header("Custom Misc Variables")]
    public bool gunDrawn = false;
    public GameObject gun;
    public GameObject bigGun;
    public GameObject fakeGun;
    public GameObject bigFakeGun;
    public ParticleSystem GunGetParticle;
    public GameObject rightClick;
    public Vector3 objectHoldOffset;
    public float gunOutSpeed = 1.75f;
    bool holdingObject = false;
    public bool playedGunGetParticle = false;


    PlayerHealth playerHealth;

    #region PROPERTIES

    /// <summary>
    /// The character's walk speed.
    /// </summary>

    public float walkSpeed
    {
        get { return _walkSpeed; }
        set { _walkSpeed = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// The character's run speed.
    /// </summary>

    public float runSpeed
    {
        get { return _runSpeed; }
        set { _runSpeed = Mathf.Max(0.0f, value); }
    }

    /// <summary>
    /// Walk input command.
    /// </summary>

    public bool run { get; private set; }

    #endregion

    public override void Awake()
    {
        base.Awake();
        Cursor.lockState = CursorLockMode.Locked;
        anim = GetComponentInChildren<Animator>();
        lookMask = LayerMask.GetMask ("Floor", "Enemy");
        playerHealth = GetComponent<PlayerHealth>();
        //transform.position = Vector3.zero;
    }

    private float GetTargetSpeed()
    {
        return run ? runSpeed : walkSpeed;
    }

    protected override Vector3 CalcDesiredVelocity()
    {
        // If using root motion and root motion is being applied (eg: grounded),
        // use animation velocity as animation takes full control

        if (useRootMotion && applyRootMotion)
            return rootMotionController.animVelocity;

        // else, convert input (moveDirection) to velocity vector
        //Vector3 temp;

        //temp = transform.forward * moveDirection.z;

        speed = GetTargetSpeed();

        if(gunDrawn){
            return moveDirection * gunOutSpeed;
        } else {
            return moveDirection * speed;
        }
        
    }

    protected override void HandleInput()
    {
        moveDirection = new Vector3
        {
            x = Input.GetAxisRaw("Horizontal"),
            y = 0.0f,
            z = Input.GetAxisRaw("Vertical")
        };

        // Transform move direction to be relative to character's current orientation

        moveDirection = transform.TransformDirection(moveDirection);

        // In this implementation, we rotate the character's (along its yaw) using the mouse

        // Smoothly rotate the character's (along its Y-axis) using the horizontal mouse input.
        if(Cursor.lockState == CursorLockMode.Locked){
            _targetYaw += Input.GetAxis("Mouse X");
        }
        _targetYaw = Utils.WrapAngle(_targetYaw);
        
        // Walk, jump inputs

        run = Input.GetButton("Fire3");
        jump = Input.GetButton("Jump");
        if((Input.GetButtonDown("Fire2") || (!gunDrawn && Input.GetButtonDown("Fire1"))) && !playerHealth.isDead){
            SetGunDrawn();
        }
    }

    protected override void Animate()
    {
        if(!playerHealth.isDead){
            bool walking = /*moveDirection.x != 0 ||*/ moveDirection.z != 0;

            //anim.SetBool ("IsWalking", walking);
            anim.SetFloat("Vertical", moveDirection.z);
            anim.SetFloat("Horizontal", moveDirection.x);
            anim.SetFloat("Direction", transform.rotation.eulerAngles.y % 360);
        }  
    }

    protected override void UpdateRotation()
    {
        // Perform character (CharacterMovement component) rotation, around its Y - axis(yaw rotation)

        _yaw = Mathf.SmoothDampAngle(_yaw, _targetYaw, ref _yawVelocity, time2TurnTarget);
        
        movement.rotation = Quaternion.Euler(0.0f, _yaw, 0.0f);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////

            // However if your third person camera handles the rotation, and you want to match the character
            // rotation to the camera's one, please use this section

            // Generate a target rotation from cameras current look direction

            //var targetRot = Quaternion.LookRotation(Camera.main.transform.forward, transform.up);

            // Interpolate character's rotation towards camera's view direction

            // var newRotation = Quaternion.Lerp(transform.rotation, targetRot, Time.deltaTime * 20f);
            // movement.rotation = Quaternion.Euler(0.0f, newRotation.eulerAngles.y, 0f);

            ///////////////////////////////////////////////////////////////////////////////////////////////////////

    }

    // protected override void UpdateRotation(){
    //     if (useRootMotion && applyRootMotion && useRootMotionRotation)
    //         {
    //             // Use animation angular velocity to rotate character

    //             Quaternion q = Quaternion.Euler(Vector3.Project(rootMotionController.animAngularVelocity, transform.up));

    //             movement.rotation *= q;
    //         }
    //         else
    //         {
    //             // Rotate towards movement direction (input)

    //             //RotateTowardsMoveDirection();

    //             lookRay = Camera.main.ScreenPointToRay(Input.mousePosition);
    //             Debug.DrawRay(lookRay.origin, lookRay.direction * range);
    //             if(Physics.Raycast (lookRay, out lookHit, range, lookMask))
    //             {
    //                 if(!playerHealth.isDead){
    //                     RotateTowards(lookHit.point - transform.position, true);
    //                     //float directionX = Vector3.Dot(transform.forward, Vector3.forward);
    //                     //float directionY = Vector3.Dot(transform.forward, Vector3.right);
    //                     //Debug.Log($"{directionX}, {directionY}");
    //                 }
    //             }
    //         }
    // }

    /// <summary>
    /// Get target speed based on character state (eg: running, walking, etc).
    /// </summary>

    public void SetGunDrawn(){
        gunDrawn = !gunDrawn;
        anim.SetBool("GunDrawn", gunDrawn);
        anim.SetTrigger("MoveGun");
        gun.SetActive(gunDrawn);
        fakeGun.SetActive(!gunDrawn);
        rightClick.SetActive(gunDrawn);
        
    }

    public void SwapGuns(){
        gun.SetActive(false);
        fakeGun.SetActive(false);
        gun = bigGun;
        fakeGun = bigFakeGun;
        if(gunDrawn){
            gun.SetActive(true);
        } else {
            fakeGun.SetActive(true);
        }
        if(!playedGunGetParticle){
            GunGetParticle.Play();
            playedGunGetParticle = true;
        }
    }

    public bool IsHolding(){
        return holdingObject;
    }

    public void HoldObject(bool holding){
        if(gunDrawn && holding){
            SetGunDrawn();
        }
        holdingObject = holding;
        
    }

    public override void OnValidate()
    {
        // Validate 'BaseCharacterController' editor exposed fields

        base.OnValidate();

        // Validate this editor exposed fields

        walkSpeed = _walkSpeed;
        runSpeed = _runSpeed;
    }

    private void OnEnable()
    {
        // Initialize vars

        _yaw = _targetYaw = transform.eulerAngles.y;
    }
}
