﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int maxHealth = 100;
    public int currentHealth;
    //public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);
    public bool godMode = false;
    public Slider healthSlider;

    Animator anim;
    AudioSource playerAudio;
    CustomCharacterController playerMovement;
    public PlayerShooting playerShooting;
    public bool isDead;
    bool damaged;

    GameManager gameManager;

    void Awake()
    {
        // Setting up the references.
        anim = GetComponentInChildren<Animator>();
        playerAudio = GetComponent<AudioSource>();
        playerMovement = GetComponent<CustomCharacterController>();
        //playerShooting = GetComponentInChildren<PlayerShooting>();
        gameManager = FindObjectOfType<GameManager>();

        ResetPlayer();
    }

    public void ResetPlayer()
    {
        // Set the initial health of the player.
        currentHealth = gameManager.GetStartingHealth();
        healthSlider.value = currentHealth;

        maxHealth = gameManager.GetMaxHealth();
        healthSlider.maxValue = gameManager.GetMaxHealth();

        playerMovement.walkSpeed = 2.5f;
        playerMovement.runSpeed = 2.5f;

        //playerMovement.enabled = true;
        //playerShooting.enabled = true;

        //anim.SetBool("IsDead", false);
    }


    void Update()
    {
        // If the player has just been damaged...
        if (damaged)
        {
            // ... set the colour of the damageImage to the flash colour.
            damageImage.color = flashColour;
        }
        // Otherwise...
        else
        {
            // ... transition the colour back to clear.
            if(damageImage){
                damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
            }
        }

        // Reset the damaged flag.
        damaged = false;
    }


    public void TakeDamage(int amount)
    {
        if (godMode)
            return;

        // Set the damaged flag so the screen will flash.
        damaged = true;

        // Reduce the current health by the damage amount.
        currentHealth -= amount;
        healthSlider.value = currentHealth;

        // Set the health bar's value to the current health.
        //healthSlider.value = currentHealth;

        // Play the hurt sound effect.
        //playerAudio.Play();

        // If the player has lost all it's health and the death flag hasn't been set yet...
        if (currentHealth <= 0 && !isDead)
        {
            // ... it should die.
            Death();
        }
    }

    public void HealthSacrificeVisualUpdate(){
        damaged = true;
        maxHealth = maxHealth * 4;
        healthSlider.maxValue = maxHealth; //Make the current health 25% of the max health
    }

    void Death()
    {
        // Set the death flag so this function won't be called again.
        isDead = true;

        // Turn off any remaining shooting effects.
        //playerShooting.DisableEffects();

        // Tell the animator that the player is dead.
        anim.SetTrigger("IsDead");

        // Set the audiosource to play the death clip and play it (this will stop the hurt sound from playing).
        playerAudio.clip = deathClip;
        playerAudio.Play();
        gameManager.IncrementDeaths();

       

        // Turn off the movement and shooting scripts.
        playerMovement.walkSpeed = 0;
        playerMovement.runSpeed = 0;
        playerShooting.enabled = false;

        StartCoroutine("ReloadLevel");
    }

    IEnumerator ReloadLevel(){
        yield return new WaitForSeconds(5);
        gameManager.ReloadLevelMethod();
    }
}
