﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Text;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.33f;

    public int bigGunDamagePerShot = 30;
    public float bigGuntimeBetweenBullets = .2f;
    public float range = 100f;
 
    public CustomCharacterController player;
    public AudioSource gunAudio;
    public List<AudioClip> audioList;
    public List<AudioClip> bigGunAudioList;
    public Transform gunEnd;
    public Transform bigGunEnd;
    public LineRenderer gunLine;
    public LineRenderer bigGunLine;
    
    //Light gunLight;
    //public Light faceLight;


    float timer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    int enemyMask;
    //ParticleSystem gunParticles;
    

    float effectsDisplayTime = 0.2f;
    GameManager gameManager;
    bool bigGun;

    private UnityAction listener;

    void Awake ()
    {
        // Create a layer mask for the Shootable layer.
        shootableMask = LayerMask.GetMask ("Shootable");
        enemyMask = LayerMask.GetMask ("Enemy");

        // Set up the references.
        //gunParticles = GetComponent<ParticleSystem> ();
        //gunLine = GetComponent <LineRenderer> ();
        gameManager = FindObjectOfType<GameManager>();
        if(gameManager && gameManager.GetBigGunStatus()){
            GiveBigGun();  
            player.SwapGuns();
        }
        //gunLight = GetComponent<Light> ();
        //faceLight = GetComponentInChildren<Light> ();

        //AdjustGrenadeStock(0);

        //listener = new UnityAction(CollectGrenade);

        //EventManager.StartListening("GrenadePickup", CollectGrenade);

        //StartPausible();
    }

    void Update ()
    {
        //if (isPaused)
            //return;

        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        if (timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            // If the Fire1 button is being press and it's time to fire...
            /*if (Input.GetButton("Fire2") && grenadeStock > 0)
            {
                // ... shoot a grenade.
                ShootGrenade();
            }

            // If the Fire1 button is being press and it's time to fire...
            else */if (Input.GetButton("Fire1"))
            {
                // ... shoot the gun.
                if(player.gunDrawn){
                    Shoot();
                }
                
            }
        }
        // // If the timer has exceeded the proportion of timeBetweenBullets that the effects should be displayed for...
        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            // ... disable the effects.
            DisableEffects ();
        }
    }


    public void DisableEffects ()
    {
        // Disable the line renderer and the light.
        gunLine.enabled = false;
        //faceLight.enabled = false;
        //gunLight.enabled = false;
    }


    void Shoot ()
    {
        // Reset the timer.
        timer = 0f;

        // Play the gun shot audioclip.
        if(bigGun){
            gunAudio.clip = audioList[Random.Range(0, bigGunAudioList.Count)];
        } else {
            gunAudio.clip = audioList[Random.Range(0, audioList.Count)];
        }
        gunAudio.Play();
        gameManager.IncrementBulletsFired();

        // Enable the lights.
        //gunLight.enabled = true;
        //faceLight.enabled = true;

        // Stop the particles from playing if they were, then start the particles.
        //gunParticles.Stop ();
        //gunParticles.Play ();

        // Enable the line renderer and set it's first position to be the end of the gun.
        gunLine.enabled = true;
        gunLine.SetPosition (0, gunEnd.position);
        if(bigGun){
            gunLine.endColor = Color.red;
            gunLine.startColor = Color.red;
        }
        

        //////////////////
        /// This raycast stuff works, uncomment if you break everything
        /////////////
        // shootRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        // // Perform the raycast against gameobjects on the shootable layer and if it hits something...
        // if(Physics.Raycast (shootRay, out shootHit, range, shootableMask, QueryTriggerInteraction.Ignore))
        // {
        //     gunLine.SetPosition (1, shootHit.point);
        //     shootRay.origin = gunEnd.position;
        //     shootRay.direction = shootHit.point - gunEnd.position;
        //     if(Physics.Raycast (shootRay, out shootHit, range, shootableMask, QueryTriggerInteraction.Ignore))
        //     {
        //         // Try and find an EnemyHealth script on the gameobject hit.
        //         EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();

        //         // If the EnemyHealth component exist...
        //         if(enemyHealth != null)
        //         {
        //             // ... the enemy should take damage.
        //             enemyHealth.TakeDamage (damagePerShot, shootHit.point);
        //         }

        //         // Set the second position of the line renderer to the point the raycast hit.
        //         gunLine.SetPosition (1, shootHit.point);
        //     } 
        // }
        // // If the raycast didn't hit anything on the shootable layer...
        // else
        // {
        //     // ... set the second position of the line renderer to the fullest extent of the gun's range.
        //     gunLine.SetPosition (1, gunEnd.position + shootRay.direction * range);
        // }
        
        shootRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        //shootRay.origin = gunEnd.position;
        //shootRay.direction = shootRay.direction - gunEnd.position;
        // Perform the raycast against gameobjects on the shootable layer from the screen center and if it hits something...
        if(Physics.Raycast (shootRay, out shootHit, range, enemyMask, QueryTriggerInteraction.Ignore) || Physics.Raycast(gunEnd.position, (gunEnd.position + shootRay.direction * range) - gunEnd.position, out shootHit, range, enemyMask, QueryTriggerInteraction.Ignore) )
        {
            gunLine.SetPosition (1, shootHit.point);

            // Try and find an EnemyHealth script on the gameobject hit.
            EnemyHealth enemyHealth = shootHit.collider.GetComponentInParent <EnemyHealth> ();

            // If the EnemyHealth component exist...
            if(enemyHealth != null)
            {
                // ... the enemy should take damage.
                enemyHealth.TakeDamage (damagePerShot, shootHit.point);
            }
        }
        else if(Physics.Raycast (shootRay, out shootHit, range, shootableMask, QueryTriggerInteraction.Ignore))// If the raycast hit anything on the shootable layer...
        {
            gunLine.SetPosition (1, shootHit.point);

        } else { //if the raycasts hit no ememies or shootable things
            // ... set the second position of the line renderer to the fullest extent of the gun's range.
            gunLine.SetPosition (1, gunEnd.position + shootRay.direction * range);
        }
    }

    public void GiveBigGun(){
        damagePerShot = bigGunDamagePerShot;
        timeBetweenBullets = bigGuntimeBetweenBullets;
        bigGun = true;
        gunEnd = bigGunEnd; 
        gunLine = bigGunLine; 
    }

    // private void ChangeGunLine(float midPoint)
    // {
    //     AnimationCurve curve = new AnimationCurve();

    //     curve.AddKey(0f, 0f);
    //     curve.AddKey(midPoint, 0.5f);
    //     curve.AddKey(1f, 1f);

    //     gunLine.widthCurve = curve;
    // }

    // public void CollectGrenade()
    // {
    //     AdjustGrenadeStock(1);
    // }

    // private void AdjustGrenadeStock(int change)
    // {
    //     grenadeStock += change;
    //     GrenadeManager.grenades = grenadeStock;
    // }

    // void ShootGrenade()
    // {
    //     AdjustGrenadeStock(-1);
    //     timer = timeBetweenBullets - grenadeFireDelay;
    //     GameObject clone = PoolManager.Pull("Grenade", transform.position, Quaternion.identity);
    //     EventManager.TriggerEvent("ShootGrenade", grenadeSpeed * transform.forward);
    //     //GameObject clone = Instantiate(grenade, transform.position, Quaternion.identity);
    //     //Grenade grenadeClone = clone.GetComponent<Grenade>();
    //     //grenadeClone.Shoot(grenadeSpeed * transform.forward);
    // }
}