﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float rotationSpeed;
    public float lowerBound, upperBound;
    public Transform target, rotationPoint, player;
    float mouseX, mouseY;


    private void Update() {
        CamControl();
    }
    void CamControl()
    {
        
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
        mouseY = Mathf.Clamp(mouseY, lowerBound, upperBound);

        rotationPoint.rotation = Quaternion.Euler(mouseY, mouseX, 0);
        player.rotation = Quaternion.Euler(0, mouseX, 0);

        if(target){
            transform.LookAt(target);
        }
    }
}
