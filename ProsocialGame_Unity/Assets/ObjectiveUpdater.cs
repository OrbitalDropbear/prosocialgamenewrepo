﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveUpdater : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if(other.CompareTag("Player")){
            FindObjectOfType<LevelManager_GasStation>().UpdateObjectivesListStranger();
            Destroy(gameObject);
        }
    }
}
